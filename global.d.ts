// Assets
declare module '*.png' {
  const content: string;
  export default content;
}

// General RN helpers
type NestedNavigatorParams<ParamList> = {
  [K in keyof ParamList]: undefined extends ParamList[K]
    ? {screen: K; params?: ParamList[K]}
    : {screen: K; params: ParamList[K]};
}[keyof ParamList];

// Forms
type SignUpFormFields =
  | 'phone '
  | 'firstName'
  | 'lastName'
  | 'email'
  | 'password';
type SignUpFormError = {
  name: SignUpFormFields;
  type: string;
  message: string;
};
type SignUpFormData<T> = {[field in SignUpFormFields]: T};

type LogInFormFields = 'login' | 'pas';
type LoginFormError = {
  name: LogInFormFields;
  type: string;
  message: string;
};
type LogInFormData<T> = {[field in LogInFormFields]: T};
