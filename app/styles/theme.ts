import {DefaultTheme} from 'react-native-paper';

export default {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    // Brand colors
    Icon: '#7B7C83',
    brandWhite: '#fff',
    brandDark: '#2D2D2D',
    brandGray: '#495057',
    brandGrayLight: '#ccc',
    primary: '#9B9B9B',
    accent: '#ffffff',
    background: 'transparent',
    text: '#ffffff',
  },
};
