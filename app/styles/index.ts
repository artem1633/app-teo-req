 // @ts-ignore
import styled from 'styled-components/native';
import SafeAreaViewLib from 'react-native-safe-area-view';
import {Text} from 'react-native-paper';

import theme from './theme';

// Base layouts
export const ScreenContent = styled.ScrollView`
  flex: 1;
`;

export const SafeAreaView = styled(SafeAreaViewLib)`
  flex: 1;
`;

export const PrimaryHeaderBlock = styled.View`
  padding: 20px 10px 0 10px;
  justify-content: space-between;
  flex-direction: row;
`;

export const BigEmoji = styled.Text`
  font-size: 70px;
`;

export const EmojiPlaceholder = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
`;

// Inputs
export const PrimaryInput = styled.Text`
  padding: 21px 10px;
  font-size: 16px;
  font-weight: 500;
  color: ${theme.colors.brandDark};
`;

// Sign Up + Sign In screen
const BottomContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

const LogoContainer = styled.View`
  flex-direction: row;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Logo = styled.Image`
  height: 58px;
  resize-mode: contain;
`;

const TermsAndConditions = styled(Text)`
  margin-top: 20px;
`;

export default {
  ScreenContent,
  SafeAreaView,
  PrimaryHeaderBlock,
  BigEmoji,
  EmojiPlaceholder,
  BottomContainer,
  LogoContainer,
  Logo,
  TermsAndConditions,
};
