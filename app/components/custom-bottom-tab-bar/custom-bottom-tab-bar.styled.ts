// @ts-ignore
import styled from 'styled-components/native';

const Container = styled.View`
  flex-direction: row;
  box-shadow: 0 4px 32px rgba(7, 8, 14, 0.05);
  border-radius: 22px;
  width: 75%;
  height: 65px;
  position: absolute;
  bottom: 20px;
  right: 12.5%;
  left: 12.5%;
`;

const TouchableOpacity = styled.TouchableOpacity`
  padding-top: 22px;
  flex: 1;
  align-items: center;
`;

const IconContainer = styled.View`
  position: relative;
`;

export default {
  Container,
  TouchableOpacity,
  IconContainer,
};
