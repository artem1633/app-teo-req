import React from 'react';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, Image, Text} from 'react-native';
// Styles
import S from './custom-bottom-tab-bar.styled';
import theme from '../../styles/theme';
import {useStore} from '../../store';
import {observer} from 'mobx-react-lite';

interface CustomBottomTabBarProps extends BottomTabBarProps {}

const CustomBottomTabBar: React.FC<CustomBottomTabBarProps> = observer(
  ({state, descriptors, navigation}) => {
    const {bottom} = useSafeAreaInsets();
    const store = useStore();
    return (
      <S.Container
        style={{
          paddingBottom: bottom,
          backgroundColor: store.common.defaultTheme.inputColor,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.3,
          shadowRadius: 4.65,

          elevation: 4,
        }}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const isFocused = state.index === index;

          const onPress = async () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (route.name === 'Applications') {
              await store.common.getApplicationList(1, store.common.status);
            }

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          const color = isFocused
            ? store.common.defaultTheme.focusedIcon
            : theme.colors.Icon;
          return (
            <S.TouchableOpacity
              key={route.key}
              accessibilityRole="button"
              // accessibilityStates={isFocused ? ['selected'] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}>
              {options.tabBarIcon && (
                <S.IconContainer>
                  {options.tabBarIcon({size: 0, focused: isFocused, color})}
                </S.IconContainer>
              )}
            </S.TouchableOpacity>
          );
        })}
        <S.TouchableOpacity
          accessibilityRole="button"
          onPress={() =>
            Alert.alert(
              'Очистить список заявок',
              '',
              [
                {
                  text: 'Нет',
                  style: 'cancel',
                },
                {
                  text: 'Очистить',
                  onPress: async () => {
                    await store.common.deleteAllApplication(
                      store.common.status,
                    );
                  },
                },
              ],
              {cancelable: false},
            )
          }>
          <S.IconContainer>
            <Image source={require('../../assets/Delete.png')} />
          </S.IconContainer>
        </S.TouchableOpacity>
      </S.Container>
    );
  },
);

export default CustomBottomTabBar;
