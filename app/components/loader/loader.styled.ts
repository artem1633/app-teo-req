// @ts-ignore
import styled from 'styled-components/native';

const Container = styled.View`
  justify-content: center;
  align-items: center;
  background-color: transparent;
`;

export default {
  Container,
};
