import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {observer} from 'mobx-react-lite';
import {Image} from 'react-native';
// Navigators
import {HomeTabParamList} from '../root-stack-navigator/root-stack-navigator.component';
// Screens
import ApplicationScreen from '../../screens/application-screen';
import KeywordScreen from '../../screens/keyword-screen';
import SettingsScreen from '../../screens/settings-screen';
// Components
import CustomBottomTabBar from '../../components/custom-bottom-tab-bar';

const Tab = createBottomTabNavigator<HomeTabParamList>();

interface HomeTabNavigatorProps {}

const HomeTabNavigator: React.FC<HomeTabNavigatorProps> = observer(() => {
  return (
    <Tab.Navigator tabBar={(props) => <CustomBottomTabBar {...props} />}>
      <Tab.Screen
        name="Applications"
        component={ApplicationScreen}
        options={{
          title: 'Applications',
          tabBarIcon: (props) => (
            <Image
              source={require('../../assets/HomeIcon.png')}
              style={{tintColor: props.color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Keyword"
        component={KeywordScreen}
        options={{
          title: 'Keyword',
          tabBarIcon: (props) => (
            <Image
              source={require('../../assets/KeywordIcon.png')}
              style={{tintColor: props.color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          title: 'Settings',
          tabBarIcon: (props) => (
            <Image
              source={require('../../assets/SettingsIcon.png')}
              style={{tintColor: props.color}}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
});

export default HomeTabNavigator;
