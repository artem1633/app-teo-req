import React from 'react';
import {observer} from 'mobx-react-lite';
import {createStackNavigator} from '@react-navigation/stack';

// Navigators
import {AuthStackParamList} from '../root-stack-navigator/root-stack-navigator.component';
// Screens
import SignInScreen from '../../screens/signin-screen';
import SignUpScreen from '../../screens/signup-screen';
import ForgotPassword from '../../screens/forgot-password-screen';
import ChangePasScreen from '../../screens/change-password-screen';
import { useStore } from "../../store";

const AuthStack = createStackNavigator<AuthStackParamList>();

interface AuthStackNavigatorProps {}

const AuthStackNavigator: React.FC<AuthStackNavigatorProps> = observer(() => {
  const store = useStore()
  return (
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="SignIn"
        component={SignInScreen}
        options={{headerShown: false}}
      />
      <AuthStack.Screen
        name="SignUp"
        component={SignUpScreen}
        options={{
          headerTitleStyle: {
            color: store.common.defaultTheme.background,
          },
          headerTransparent: true,
        }}
      />
      <AuthStack.Screen
        name="Forgot"
        component={ForgotPassword}
        options={{headerShown: false}}
      />
      <AuthStack.Screen
        name="ChangePas"
        component={ChangePasScreen}
        options={{headerShown: false}}
      />
    </AuthStack.Navigator>
  );
});

export default AuthStackNavigator;
