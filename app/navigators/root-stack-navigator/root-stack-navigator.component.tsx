import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from 'react';
import {observer} from 'mobx-react-lite';
import {
  NavigationContainer,
  NavigationContainerRef,
  RouteProp,
} from '@react-navigation/native';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import RNBootSplash from 'react-native-bootsplash';

// // Navigators
import HomeTabNavigator from '../home-tab-navigator';
import AuthStackNavigator from '../auth-stack-navigator/auth-stack-navigator.component';
// // Screens
import InitialScreen from '../../screens/initial-screen';
// import CreateKeyScreenComponent from '../../screens/keyword-screen/components/create-key';
import ChanelScreen from '../../screens/chanel-screen';
import FeedbackScreen from '../../screens/feedback-screen';
import {useStore} from '../../store';
import {Image, StatusBar} from 'react-native';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import messaging from '@react-native-firebase/messaging';

// Types
export type RootStackParamList = {
  Initial: undefined;
  Home: undefined;
  Auth: undefined;
  CreateKey: undefined;
  Chanel: undefined;
  Feedback: undefined;
};

export type HomeTabParamList = {
  Applications: undefined;
  Keyword: undefined;
  Settings: undefined;
};

export type AuthStackParamList = {
  SignIn: undefined;
  SignUp: undefined;
  Home: undefined;
  Forgot: undefined;
  ChangePas: {login: string} | undefined;
};

const RootStack = createStackNavigator<RootStackParamList>();

// SignUp screen
type SignUpScreenRouteProp = RouteProp<AuthStackParamList, 'SignUp'>;

type SignUpScreenNavigationProp = StackNavigationProp<
  AuthStackParamList,
  'SignUp'
>;

export type SignUpScreenProps = {
  route: SignUpScreenRouteProp;
  navigation: SignUpScreenNavigationProp;
};

type SignInScreenRouteProp = RouteProp<AuthStackParamList, 'SignIn'>;

type SignInScreenNavigationProp = StackNavigationProp<
  AuthStackParamList,
  'SignIn'
>;

export type SignInScreenProps = {
  route: SignInScreenRouteProp;
  navigation: SignInScreenNavigationProp;
};

// Initial stack screens
export type InitialStackParamList = {
  Initial: undefined;
  Auth: undefined;
  Home: undefined;
};

type InitialScreenRouteProp = RouteProp<InitialStackParamList, 'Initial'>;

type InitialScreenNavigationProp = StackNavigationProp<
  InitialStackParamList,
  'Initial'
>;

export type InitialScreenProps = {
  route: InitialScreenRouteProp;
  navigation: InitialScreenNavigationProp;
};
// Applications stack screens
export type ApplicationsStackParamList = {
  Applications: undefined;
};

type ApplicationsScreenRouteProp = RouteProp<
  ApplicationsStackParamList,
  'Applications'
>;

type ApplicationsScreenNavigationProp = StackNavigationProp<
  ApplicationsStackParamList,
  'Applications'
>;

export type ApplicationsScreenProps = {
  route: ApplicationsScreenRouteProp;
  navigation: ApplicationsScreenNavigationProp;
};
// Keyword stack screens
export type KeywordStackParamList = {
  Keyword: undefined;
  CreateKey: {name: string} | undefined;
};

type KeywordScreenRouteProp = RouteProp<KeywordStackParamList, 'Keyword'>;

type KeywordScreenNavigationProp = StackNavigationProp<
  KeywordStackParamList,
  'Keyword'
>;

export type KeywordScreenProps = {
  route: KeywordScreenRouteProp;
  navigation: KeywordScreenNavigationProp;
};
// Settings stack screens
export type SettingsStackParamList = {
  Settings: undefined;
  Chanel: undefined;
  Feedback: undefined;
};

type SettingsScreenRouteProp = RouteProp<SettingsStackParamList, 'Settings'>;

type SettingsScreenNavigationProp = StackNavigationProp<
  SettingsStackParamList,
  'Settings'
>;

export type SettingsScreenProps = {
  route: SettingsScreenRouteProp;
  navigation: SettingsScreenNavigationProp;
};
interface RootStackNavigatorProps {}

const RootStackNavigator: React.FC<RootStackNavigatorProps> = observer(() => {
  const [initialized, setInitialized] = useState(false);
  const store = useStore();
  const navigatorRef = useRef<NavigationContainerRef | null>(null);
  // TODO: add real types
  const navigate = useCallback((name: string, params?: any) => {
    navigatorRef.current?.navigate(name, params);
  }, []);

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      await store.auth.setFCM(fcmToken);
    }
  };

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      await getFcmToken();
      console.log('Authorization status:', authStatus);
    }
  };

  useLayoutEffect(() => {
    changeNavigationBarColor(store.common.defaultTheme.background, true, true);
  }, [store.common.defaultTheme.background]);

  useEffect(() => {
    store.common.getTheme().then();
    store.common.getBase().then();
    store.auth.getNewUser().then();
  }, [store.auth, store.common]);

  useEffect(() => {
    RNBootSplash.show();
    store.auth.singInWithToken().then(() => {
      RNBootSplash.hide({duration: 500});
    });
  }, [store.auth, initialized]);

  useEffect(() => {
    requestUserPermission().then();
  }, [requestUserPermission]);

  useEffect(() => {
    if (store.auth.isSignedIn) {
      store.common.getMyKey().then();
    }
  }, [store.auth.isSignedIn, store.common]);

  useEffect(() => {
    setTimeout(() => {
      setInitialized(true);
    }, 3000);
  }, []);

  useEffect(() => {
    store.common.getApplicationList(1, 0).then(() => {});
  }, [store.common]);

  if (!initialized) {
    return <InitialScreen navigate={navigate} />;
  }

  console.log(store.auth.newUser);

  return (
    <>
      <NavigationContainer ref={navigatorRef}>
        <StatusBar
          barStyle={
            store.common.defaultTheme.background === 'black'
              ? 'light-content'
              : 'dark-content'
          }
          backgroundColor={
            store.common.defaultTheme.background === 'black' ? 'black' : 'white'
          }
        />
        {store.auth.isSignedIn &&
        (store.auth.newUser === 'false' || !store.auth.newUser) ? (
          <RootStack.Navigator>
            <RootStack.Screen
              name="Home"
              component={HomeTabNavigator}
              options={{
                headerShown: false,
              }}
            />
            <RootStack.Screen
              name="Chanel"
              component={ChanelScreen}
              options={{
                title: 'Мои каналы',
                headerBackImage: () => (
                  <Image
                    source={
                      store.common.defaultTheme.background === 'black'
                        ? require('../../assets/BackBtn.png')
                        : require('../../assets/BlackBack.png')
                    }
                    style={{
                      marginLeft:
                        store.common.defaultTheme.background === 'black'
                          ? 13
                          : 20,
                    }}
                  />
                ),
                headerTitleStyle: {
                  color: store.common.defaultTheme.background,
                },
                headerTransparent: true,
              }}
            />
            <RootStack.Screen
              name="Feedback"
              component={FeedbackScreen}
              options={{
                headerBackImage: () => (
                  <Image
                    source={
                      store.common.defaultTheme.background === 'black'
                        ? require('../../assets/BackBtn.png')
                        : require('../../assets/BlackBack.png')
                    }
                    style={{
                      marginLeft:
                        store.common.defaultTheme.background === 'black'
                          ? 13
                          : 20,
                    }}
                  />
                ),
                headerTitleStyle: {
                  color: store.common.defaultTheme.background,
                },
                headerTransparent: true,
              }}
            />
          </RootStack.Navigator>
        ) : (
          <AuthStackNavigator />
        )}
      </NavigationContainer>
      {/*<Menu opened={store.common.showDelete}>*/}
      {/*  <MenuTrigger*/}
      {/*    text="Select option"*/}
      {/*    customStyles={{*/}
      {/*      triggerWrapper: {*/}
      {/*        display: 'none',*/}
      {/*      },*/}
      {/*    }}*/}
      {/*  />*/}
      {/*  <MenuOptions*/}
      {/*    customStyles={{*/}
      {/*      optionsWrapper: {*/}
      {/*        position: 'absolute',*/}
      {/*        backgroundColor:*/}
      {/*          store.common.defaultTheme.background === 'black'*/}
      {/*            ? '#1B1814'*/}
      {/*            : '#fff',*/}
      {/*        borderRadius: 8,*/}
      {/*        width: 170,*/}
      {/*        height: 'auto',*/}
      {/*        alignItems: 'center',*/}
      {/*        shadowColor: '#000',*/}
      {/*        shadowOffset: {*/}
      {/*          width: 0,*/}
      {/*          height: 4,*/}
      {/*        },*/}
      {/*        shadowOpacity: 0.3,*/}
      {/*        shadowRadius: 4.65,*/}
      {/*        bottom: Dimensions.get('window').height / 2,*/}
      {/*        left: '62.5%',*/}
      {/*        elevation: 2,*/}
      {/*      },*/}
      {/*    }}>*/}
      {/*    <MenuOption*/}
      {/*      disabled={true}*/}
      {/*      style={{*/}
      {/*        borderBottomWidth: 1,*/}
      {/*        borderBottomColor: '#9B9B9B',*/}
      {/*        width: '100%',*/}
      {/*        justifyContent: 'center',*/}
      {/*        alignItems: 'center',*/}
      {/*      }}>*/}
      {/*      <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}>*/}
      {/*        Очистить список заявок?*/}
      {/*      </Text>*/}
      {/*    </MenuOption>*/}
      {/*    <MenuOption*/}
      {/*      onSelect={async () => {*/}
      {/*        await store.common.deleteAllApplication(store.common.status);*/}
      {/*        await store.common.getApplicationList(1, store.common.status);*/}
      {/*      }}*/}
      {/*      style={{*/}
      {/*        borderBottomWidth: 1,*/}
      {/*        borderBottomColor: '#9B9B9B',*/}
      {/*        width: '100%',*/}
      {/*        justifyContent: 'center',*/}
      {/*        alignItems: 'center',*/}
      {/*      }}>*/}
      {/*      <Text style={{color: '#E64646', fontSize: 16}}>Очистить</Text>*/}
      {/*    </MenuOption>*/}
      {/*    <MenuOption*/}
      {/*      onSelect={() => store.common.setShowDelete(false)}*/}
      {/*      style={{*/}
      {/*        borderBottomWidth: 1,*/}
      {/*        borderBottomColor: '#9B9B9B',*/}
      {/*        width: '100%',*/}
      {/*        justifyContent: 'center',*/}
      {/*        alignItems: 'center',*/}
      {/*      }}>*/}
      {/*      <Text*/}
      {/*        style={{*/}
      {/*          color: store.common.defaultTheme.textColor,*/}
      {/*          fontSize: 16,*/}
      {/*        }}>*/}
      {/*        Нет*/}
      {/*      </Text>*/}
      {/*    </MenuOption>*/}
      {/*  </MenuOptions>*/}
      {/*</Menu>*/}
    </>
  );
});

export default RootStackNavigator;
