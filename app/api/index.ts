import {map} from 'rxjs/operators';
import camelize from 'camelize';
import snakeize from 'snakeize';

import {APP_API} from '../config';
import {createHttpObservable} from '../utils/create-http-observable';
import axios from 'axios';

export function signIn(data: any) {
  return axios
    .post(`https://app.teo-req.ru/api/v1/login`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function newCode(data: any) {
  return axios
    .post(`https://app.teo-req.ru/api/v1/new-code`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function signUp(data: any) {
  return axios
    .post(`https://app.teo-req.ru/api/v1/reg`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function signInWithToken(token: any) {
  return axios
    .post(`https://app.teo-req.ru/api/v1/login`, token, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}

export function setFCM(token: any) {
  return axios
    .post(`https://app.teo-req.ru/api/v1/set-fcm`, token, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}

export function getApplicationList(data: any) {
  return axios
    .post(`${APP_API}/list`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function getMyKey(accessToken: any) {
  return axios
    .post(`${APP_API}/my-key`, accessToken, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function addKey(data: any) {
  return axios
    .post(`${APP_API}/add-key`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function changeReq(data: any) {
  return axios
    .post(`${APP_API}/change-req`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function newPas(data: any) {
  return axios
    .post(`${APP_API}/new-pas`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
export function deleteApplication(data: any) {
  return axios
    .post(`${APP_API}/del-req`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => camelize(res));
}
// function createRequestInit(
//   method: string,
//   accessToken?: string,
//   body?: object,
// ) {
//   const init: RequestInit = {
//     method,
//     headers: {
//       'Content-Type': 'application/json',
//     },
//   };
//   if (body) {
//     init.body = JSON.stringify(snakeize(body));
//   }
//   if (accessToken) {
//     // @ts-ignore
//     init.headers.Authorization = `Basic ${accessToken}`;
//   }
//
//   return init;
// }

export default {
  signIn,
};
