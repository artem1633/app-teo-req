import {of} from 'rxjs';
import {delay, catchError} from 'rxjs/operators';

import {
  addKey,
  changeReq,
  getApplicationList,
  getMyKey,
  signIn,
  signInWithToken,
  signUp,
  newPas,
  deleteApplication,
  setFCM,
  newCode,
} from '../api';

export default class AuthService {
  signIn(data: any) {
    return signIn(data);
  }

  newCode(data: any) {
    return newCode(data);
  }

  signUp(data: any) {
    return signUp(data);
  }
  signInWithToken(token: any) {
    return signInWithToken(token);
  }
  getApplicationList(data: any) {
    return getApplicationList(data);
  }
  getMyKey(accessToken: any) {
    return getMyKey(accessToken);
  }
  addKey(data: any) {
    return addKey(data);
  }
  changeReq(data: any) {
    return changeReq(data);
  }
  newPas(data: any) {
    return newPas(data);
  }
  deleteApplication(data: any) {
    return deleteApplication(data);
  }
  setFCM(data: any) {
    return setFCM(data);
  }
}
