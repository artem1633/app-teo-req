import {action, observable} from 'mobx';
import AuthService from '../services/auth-service';
import DataStorage from '../utils/data-storage';

class CommonStore {
  authService: AuthService;
  @observable isLoading: boolean = false;
  @observable applicationList: any[] = [];
  @observable myKey: any | undefined;
  @observable status: number = 0;
  @observable base: boolean = true;
  @observable showDelete: boolean = false;
  @observable defaultTheme: any = {
    background: '#FDFDFD',
    authBtnColor: 'black',
    textColor: 'black',
    textAuthColor: '#2F6DB5',
    inputColor: '#fff',
    textInputColor: 'black',
    singUpText: '#979797',
    primary: '#9B9B9B',
    placeholder: '#9B9B9B',
    focusedIcon: 'black',
  };
  constructor() {
    this.authService = new AuthService();
  }
  @action
  async getApplicationList(page: number, status: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('page', page);
        formData.append('status', status);
        const list = await this.authService.getApplicationList(formData);
        this.setApplicationList(list.data);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async getApplicationNextList(page: number, status: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('page', page);
        formData.append('status', status);
        const list = await this.authService.getApplicationList(formData);
        const update = [...this.applicationList, ...list.data];
        const getUnique = (arr: any[], index: string | number) => {
          return arr
            .map((e) => e[index])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter((e: any) => arr[e])
            .map((e: any) => arr[e]);
        };
        this.setApplicationList(getUnique(update, 'id'));
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async changeReq(id: string, status: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('id', id);
        formData.append('status', status);
        await this.authService.changeReq(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async getMyKey() {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        const myKey = await this.authService.getMyKey(formData);
        this.setMyKey(myKey);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addSignUpKeyOne(data: any) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyOne', JSON.stringify(data));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addSignUpKeyMinus(data: any) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyMinus', JSON.stringify(data));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addKeyOne(data: any) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyOne.push({
          id: (Math.random().toString(16) + '00000000000000000').slice(
            2,
            12 + 2,
          ),
          value: data,
        });
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyOne', JSON.stringify(this.myKey.data.keyOne));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteKeyOne(id: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyOne = this.myKey.data.keyOne.filter(
          (item: {id: number}) => id !== item.id,
        );
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyOne', JSON.stringify(this.myKey.data.keyOne));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async editKeyOne(id: number, value: string) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyOne = this.myKey.data.keyOne.find(
          (item: {id: number; value: string}) => {
            if (item.id === id) {
              return (item.value = value);
            }
          },
        );
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyOne', JSON.stringify(this.myKey.data.keyOne));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteKeyMinus(id: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyMinus = this.myKey.data.keyMinus.filter(
          (item: {id: number}) => id !== item.id,
        );
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyMinus', JSON.stringify(this.myKey.data.keyMinus));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addKeyMinus(data: any) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyMinus.push({
          id: (Math.random().toString(16) + '00000000000000000').slice(
            2,
            12 + 2,
          ),
          value: data,
        });
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyMinus', JSON.stringify(this.myKey.data.keyMinus));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addKeyChanel(data: any, common_base_searchable: boolean = false) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyChanel.push({
          url: data,
        });
        let newArray = this.myKey.data.keyChanel.map((item: any) => ({
          value: item.url,
        }));
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyChanel', JSON.stringify(newArray));
        if (common_base_searchable) {
          formData.append('common_base_searchable', common_base_searchable);
        }
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async addKeyChanelBlack(data: any) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyChanelBlack.push({
          url: data,
        });
        let newArray = this.myKey.data.keyChanelBlack.map((item: any) => ({
          value: item.url,
        }));
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyChanelBlack', JSON.stringify(newArray));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteKeyChanel(id: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyChanel = this.myKey.data.keyChanel.filter(
          (item: any, index: number) => id !== index,
        );
        let newArray = this.myKey.data.keyChanel.map((item: any) => ({
          value: item.url,
        }));
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyChanel', JSON.stringify(newArray));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err);
    }
  }
  @action
  async deleteKeyChanelApplication(value: string) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        if (
          this.myKey.data.keyChanel.find(
            (item: {url: string}) => item.url === value,
          )
        ) {
          this.myKey.data.keyChanel = this.myKey.data.keyChanel.filter(
            (item: any) => value !== item.url,
          );
          const formData = new FormData();
          formData.append('token', accessToken);
          formData.append(
            'keyChanel',
            JSON.stringify(this.myKey.data.keyChanel),
          );
          await this.authService.addKey(formData);
        }
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteKeyChanelBlack(id: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.myKey.data.keyChanelBlack = this.myKey.data.keyChanelBlack.filter(
          (item: any, index: number) => id !== index,
        );
        let newArray = this.myKey.data.keyChanelBlack.map((item: any) => ({
          value: item.url,
        }));
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('keyChanelBlack', JSON.stringify(newArray));
        await this.authService.addKey(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteApplication(id: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        this.applicationList = this.applicationList.filter(
          (item: any) => id !== item.id,
        );
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('id', id);
        await this.authService.deleteApplication(formData);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async deleteAllApplication(status: number) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('status', status);
        await this.authService.deleteApplication(formData);
        this.setApplicationList([]);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  setMyKey(myKey: any) {
    this.myKey = myKey;
  }
  @action
  setLoading(value: boolean) {
    this.isLoading = value;
  }
  @action
  setApplicationList(list: any[]) {
    this.applicationList = list;
  }
  @action
  setShowDelete(show: boolean) {
    this.showDelete = show;
  }
  @action
  setStatus(status: number) {
    this.status = status;
  }
  @action
  async getTheme() {
    const theme = await DataStorage.getTheme();
    if (theme) {
      await this.setTheme(JSON.parse(theme));
    }
  }
  @action
  async setTheme(theme: any) {
    this.defaultTheme = theme;
    await DataStorage.storeTheme(theme);
  }
  @action
  async getBase() {
    const base = await DataStorage.getItem('chanelBase');
    if (base) {
      console.log(base);
      await this.setBase(JSON.parse(base));
    }
  }
  @action
  async setBase(base: any) {
    this.base = base;
    await DataStorage.storeItem('chanelBase', base);
  }
}

export default CommonStore;
