import {observable, action, computed} from 'mobx';
import DataStorage from '../utils/data-storage';
import AuthService from '../services/auth-service';

class AuthStore {
  authService: AuthService;
  @observable user: any | undefined = undefined;
  @observable code: any | undefined = undefined;
  @observable signUpError: any | undefined = undefined;
  @observable auth:
    | {
        result: boolean | string;
        new: boolean;
      }
    | undefined;
  @observable newUser: boolean | undefined | string = undefined;

  constructor() {
    this.authService = new AuthService();
  }

  @computed
  get isSignedIn() {
    return this.user !== undefined && !this.user.error;
  }

  @action
  async singIn(data: any) {
    try {
      const user = await this.authService.signIn(data);
      this.setUser(user.data);
      await DataStorage.storeAccessToken(user.data.token);
      await DataStorage.storeRefreshToken(user.data.token);
    } catch (err) {
      console.log('$err', err.message);
    }
  }

  @action
  async newCode(data: any) {
    try {
      const code = await this.authService.newCode(data);
      if (code) {
        this.setAuth({result: code.data.result, new: code.data.new});
        await DataStorage.storeNewUser(code.data.new);
      }
    } catch (err) {
      console.log('$err', err);
    }
  }

  @action
  async getNewUser() {
    const newUser = await DataStorage.getNewUser();
    this.setNewUser(newUser);
  }

  @action
  setAuth(result: any) {
    this.auth = result;
  }

  @action
  setNewUser(result: any) {
    this.newUser = result;
  }

  @action
  async singUp(data: any) {
    try {
      const singUp = await this.authService.signUp(data);
      if (singUp) {
        await DataStorage.storeAccessToken(singUp.data.token);
        await DataStorage.storeRefreshToken(singUp.data.token);
        this.setSignUp(singUp);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async singInWithToken() {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        const user = await this.authService.signInWithToken(formData);
        this.setUser(user.data);
      } else {
        this.setUser(undefined);
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async setFCM(fcmToken: string) {
    try {
      const accessToken = await DataStorage.getAccessToken();
      if (accessToken) {
        const formData = new FormData();
        formData.append('token', accessToken);
        formData.append('fcm_token', fcmToken);
        const test = await this.authService.setFCM(formData);
        console.log(test, 'test');
      }
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async newPas() {
    try {
      const formData = new FormData();
      formData.append('code', true);
      const code = await this.authService.newPas(formData);
      this.setCode(code.data.code);
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async changePas(pas: string, login: string) {
    try {
      const formData = new FormData();
      formData.append('pas', pas);
      formData.append('login', login);
      await this.authService.newPas(formData);
    } catch (err) {
      console.log('$err', err.message);
    }
  }
  @action
  async signOut() {
    this.setUser(undefined);
    this.setAuth(undefined);
    await DataStorage.removeRefreshToken();
    await DataStorage.removeAccessToken();
    return true;
  }
  @action
  setUser(user: any | undefined) {
    this.user = user;
  }
  @action
  setSignUp(signUpError: any | undefined) {
    this.signUpError = signUpError;
  }
  @action
  setCode(code: any | undefined) {
    this.code = code;
  }
}

export default AuthStore;
