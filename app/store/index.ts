import {createContext, useContext} from 'react';

import AuthStore from './auth-store';
import CommonStore from './common-store';

export class RootStore {
  auth: AuthStore;
  common: CommonStore;

  constructor() {
    this.auth = new AuthStore();
    this.common = new CommonStore();
  }
}

const StoreContext = createContext<RootStore>({} as RootStore);
export const StoreProvider = StoreContext.Provider;

export const useStore = () => {
  const store = useContext(StoreContext);
  if (!store) {
    throw new Error('useStore must be used within a StoreProvider.');
  }
  return store;
};
