// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet, TextProps} from 'react-native';
import {Text} from 'react-native-paper';

const Container = styled.View`
  padding-left: 31px;
  padding-right: 28px;
  padding-top: 28px;
`;
const Heading = styled(Text)`
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  margin-bottom: 28px;
`;
const UserInfo = styled.View`
  justify-content: center;
  margin-bottom: 18px;
`;
const Avatar = styled.Image`
  width: 69px;
  height: 69px;
  border-radius: 50px;
  resize-mode: contain;
  position: absolute;
`;
const Exit = styled.TouchableOpacity`
  margin-left: 95%;
  position: absolute;
`;
const Name = styled(Text)`
  height: 22px;
  font-style: normal;
  font-weight: 600;
  font-size: 17px;
  line-height: 22px;
  letter-spacing: -0.408px;
  padding-left: 77px;
`;
const Username = styled(Text)`
  height: 21px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #9b9b9b;
  padding-left: 77px;
  margin-bottom: 2px;
`;
const Subscribe = styled(Text)`
  height: 16px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #f79827;
  padding-left: 77px;
`;
const Title = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 17px;
  line-height: 22px;
  letter-spacing: -0.408px;
  margin-bottom: 3px;
`;
const SubTitle = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #9b9b9b;
  padding-bottom: 12px;
`;
export const styles = StyleSheet.create({
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#9B9B9B',
    marginBottom: 17,
  },
  paddingBottom: {
    paddingBottom: 10,
  },
});

export default {
  Heading,
  Container,
  UserInfo,
  Avatar,
  Name,
  Username,
  Subscribe,
  Exit,
  Title,
  SubTitle,
};
