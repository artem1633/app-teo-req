import React from 'react';

//Styles
import GS from '../../styles';
import S, {styles} from './settings-screen.styled';

import {observer} from 'mobx-react-lite';
//Store
import {useStore} from '../../store';
//Components
import Icon from '../../components/icon';
import {Image, TouchableOpacity, Platform, Switch} from 'react-native';
//Props
import {SettingsScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';
import changeNavigationBarColor from 'react-native-navigation-bar-color';

const SettingsScreen: React.FC<SettingsScreenProps> = observer(
  ({navigation}) => {
    const store = useStore();
    const defaultTheme =
      store.common.defaultTheme.background === 'black'
        ? {
            background: '#FDFDFD',
            authBtnColor: 'black',
            textColor: 'black',
            textAuthColor: '#2F6DB5',
            inputColor: '#fff',
            textInputColor: 'black',
            singUpText: '#979797',
            primary: '#9B9B9B',
            placeholder: '#9B9B9B',
            focusedIcon: 'black',
          }
        : {
            background: 'black',
            authBtnColor: '#F79827',
            textColor: 'white',
            textAuthColor: '#F79827',
            inputColor: '#2D2D2D',
            textInputColor: 'white',
            singUpText: '#979797',
            primary: '#9B9B9B',
            placeholder: '#9B9B9B',
            focusedIcon: 'white',
          };
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <S.Container>
          <S.Heading style={{color: store.common.defaultTheme.textColor}}>
            Профиль
          </S.Heading>
          <S.UserInfo>
            {store.auth.user.avatar ? (
              <S.Avatar source={store.auth.user.avatar} />
            ) : (
              <Image
                source={require('../../assets/Image.png')}
                style={{
                  resizeMode: 'contain',
                  width: 65,
                  height: 65,
                  position: 'absolute',
                  backgroundColor:
                    store.common.defaultTheme.background === 'black'
                      ? 'black'
                      : 'white',
                }}
              />
            )}
            <S.Name style={{color: store.common.defaultTheme.textColor}}>
              {store.auth.user.name}
            </S.Name>
            <S.Username>{store.auth.user.username || '@username'}</S.Username>
            {/*<S.Subscribe>Taриф: "Базовый"</S.Subscribe>*/}
            <S.Exit
              onPress={async () => {
                await store.auth.signOut();
              }}>
              <Image
                source={
                  store.common.defaultTheme.background === 'black'
                    ? require('../../assets/Exit.png')
                    : require('../../assets/LogoutBlack.png')
                }
              />
            </S.Exit>
          </S.UserInfo>
          <TouchableOpacity
            style={styles.border}
            onPress={() => navigation.navigate('Chanel')}>
            <S.Title style={{color: store.common.defaultTheme.textColor}}>
              Мои каналы
            </S.Title>
            <S.SubTitle>Настройки групп и каналов для поиска заявок</S.SubTitle>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.border,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              flex: 0,
              paddingTop: 0,
              paddingBottom: 10,
            }}>
            <S.Title
              style={{
                color: store.common.defaultTheme.textColor,
              }}>
              {store.common.defaultTheme.background === 'black'
                ? 'Темная тема'
                : 'Светлая тема'}
            </S.Title>
            <Switch
              trackColor={{false: '#000', true: '#fff'}}
              thumbColor={
                store.common.defaultTheme.background === 'black'
                  ? '#fff'
                  : '#000000'
              }
              ios_backgroundColor="#3e3e3e"
              onValueChange={async () => {
                await store.common.setTheme(defaultTheme);
                Platform.OS === 'android'
                  ? await changeNavigationBarColor(
                      store.common.defaultTheme.background,
                      true,
                      true,
                    )
                  : null;
              }}
              value={store.common.defaultTheme.background === 'black'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.border}
            onPress={() => navigation.navigate('Feedback')}>
            <S.Title
              style={{
                ...styles.paddingBottom,
                color: store.common.defaultTheme.textColor,
              }}>
              О приложении
            </S.Title>
          </TouchableOpacity>
        </S.Container>
      </GS.SafeAreaView>
    );
  },
);

export default SettingsScreen;
