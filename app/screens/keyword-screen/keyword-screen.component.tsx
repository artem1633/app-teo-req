import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Alert,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  Text,
} from 'react-native';
//Styles
import GS from '../../styles';
import S from './keyword-screen.styled';
//Store
import {useStore} from '../../store';
//Components
import Icon from '../../components/icon';
import {observer} from 'mobx-react-lite';
//Props
import {KeywordScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';

const KeywordScreen: React.FC<KeywordScreenProps> = observer(() => {
  const store = useStore();
  const [section, setSection] = useState<string>('keyOne');
  const [value, onChangeText] = useState<string>('');
  const onChange = (event: {
    nativeEvent: {eventCount: any; target: any; text: any};
  }) => {
    const {text} = event.nativeEvent;
    onChangeText(text);
  };
  const addKey = useCallback(async () => {
    if (/^\s*$/.test(value)) {
      Alert.alert('Заполните поле');
    } else {
      section === 'keyOne'
        ? await store.common.addKeyOne(value)
        : await store.common.addKeyMinus(value);
      onChangeText('');
    }
  }, [section, store.common, value]);
  const deleteKey = useCallback(
    async (id: number) => {
      section === 'keyOne'
        ? await store.common.deleteKeyOne(id)
        : await store.common.deleteKeyMinus(id);
    },
    [section, store.common],
  );
  const renderItem = useCallback(
    ({item}) => {
      return (
        <S.KeyContainer
          key={item.id}
          style={{
            backgroundColor: store.common.defaultTheme.inputColor,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 4.65,

            elevation: 2,
          }}>
          <S.KeyText style={{color: store.common.defaultTheme.textColor}}>
            {item.value}
          </S.KeyText>
          {/*<S.EditKey>*/}
          {/*  <Image*/}
          {/*    source={require('../../assets/editKey.png')}*/}
          {/*    style={{resizeMode: 'contain'}}*/}
          {/*  />*/}
          {/*</S.EditKey>*/}
          <S.DeleteKey onPress={() => deleteKey(item.id)}>
            <Image
              source={require('../../assets/Shape.png')}
              style={{resizeMode: 'contain'}}
            />
          </S.DeleteKey>
        </S.KeyContainer>
      );
    },
    [
      deleteKey,
      store.common.defaultTheme.inputColor,
      store.common.defaultTheme.textColor,
    ],
  );
  const keyExtractor = useCallback((item) => `${item.id}`, []);
  const listRef = useRef<FlatList>(null);
  const listOffset = useRef<number | null>(null);
  const handleScroll = (e: any) => {
    listOffset.current = e.nativeEvent.contentOffset.y;
  };

  useEffect(() => {
    store.common.getMyKey().then();
  }, [store.common]);
  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <S.Container>
          <S.Heading style={{color: store.common.defaultTheme.textColor}}>
            Ключевик
          </S.Heading>
          <S.SectionContainer>
            <S.SectionText
              style={{
                color:
                  section === 'keyOne'
                    ? store.common.defaultTheme.textColor
                    : '#818C99',
                paddingVertical: 4,
              }}
              isActive={section === 'keyOne'}
              onPress={async () => {
                setSection('keyOne');
                await store.common.getMyKey();
              }}>
              Положительные
            </S.SectionText>
            <S.SectionText
              style={{
                color:
                  section === 'keyMinus'
                    ? store.common.defaultTheme.textColor
                    : '#818C99',
                paddingVertical: 4,
              }}
              isActive={section === 'keyMinus'}
              onPress={async () => {
                setSection('keyMinus');
                await store.common.getMyKey();
              }}>
              Отрицательные
            </S.SectionText>
          </S.SectionContainer>
          <S.InputContainer>
            <S.Input
              placeholder={'Введите название ключевика'}
              placeholderTextColor={store.common.defaultTheme.placeholder}
              style={{
                backgroundColor: store.common.defaultTheme.inputColor,
                color: store.common.defaultTheme.textColor,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.3,
                shadowRadius: 4.65,

                elevation: 4,
              }}
              value={value}
              onChange={onChange}
            />
            <S.AddKey
              onPress={addKey}
              style={{
                backgroundColor: '#F79827',
                elevation: 4,
              }}>
              <Image
                source={require('../../assets/right--v1.png')}
                style={{resizeMode: 'contain', width: 30}}
              />
            </S.AddKey>
          </S.InputContainer>
          {store.common.myKey.data.keyOne ||
          store.common.myKey.data.keyMinus ? (
            <S.FlatList
              ref={listRef}
              data={
                section === 'keyOne'
                  ? store.common.myKey.data.keyOne.slice().reverse()
                  : store.common.myKey.data.keyMinus.slice().reverse()
              }
              keyExtractor={keyExtractor}
              onScroll={handleScroll}
              renderItem={renderItem}
            />
          ) : null}
        </S.Container>
      </KeyboardAvoidingView>
    </GS.SafeAreaView>
  );
});

export default KeywordScreen;
