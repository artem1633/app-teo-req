// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';

const Container = styled.View`
  padding-left: 30px;
  padding-right: 30px;
  padding-top: 32px;
`;
const Heading = styled(Text)`
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  color: #ffffff;
  margin-bottom: 28px;
`;
export const styles = StyleSheet.create({});

export default {
  Heading,
  Container,
};
