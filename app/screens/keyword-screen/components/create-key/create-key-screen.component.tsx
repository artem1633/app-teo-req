import React from 'react';
import {observer} from 'mobx-react-lite';
//Style
import GS from '../../../../styles';
import S from './create-key-screen.styled';
//Props
import {KeywordScreenProps} from '../../../../navigators/root-stack-navigator/root-stack-navigator.component';
//Theme
import {DefaultTheme, TextInput} from 'react-native-paper';
//Store
import {useStore} from '../../../../store';

import {Controller, useForm} from 'react-hook-form';
//Styles
import {styles} from '../../../signin-screen/signin-screen.styled';
//Components
import ScrollKeyboardLayout from '../../../../components/scroll-keyboard-layout';

const CreateKeyScreenComponent: React.FC<KeywordScreenProps> = observer(
  ({navigation, route}) => {
    const store = useStore();
    const {
      control,
      handleSubmit,
      formState: {isSubmitting},
    } = useForm({
      defaultValues: {
        keyOne: '1@1.ru',
        keyMinus: '',
        keyChanel: '',
        keyChanelBlack: '',
      },
    });
    const inputStyle = {
      ...DefaultTheme,
      colors: {
        ...DefaultTheme.colors,
        primary: '#9B9B9B',
        accent: '#ffffff',
        background: 'transparent',
        text: '#ffffff',
        placeholder: '#9B9B9B',
      },
    };
    return (
      <GS.SafeAreaView>
        <ScrollKeyboardLayout>
          <S.Container>
            <S.Heading>Создание ключевика</S.Heading>
            <Controller
              name="keyOne"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  label="Название"
                  placeholderTextColor="white"
                  mode="flat"
                  theme={inputStyle}
                  style={{...styles.input, marginBottom: 24}}
                  onChangeText={(value: string) => {
                    props.onChange(value);
                  }}
                />
              )}
            />
            <Controller
              name="keyOne"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  label="Слова через запятую"
                  placeholderTextColor="white"
                  mode="flat"
                  theme={inputStyle}
                  style={{...styles.input, marginBottom: 24}}
                  onChangeText={(value: string) => {
                    props.onChange(value);
                  }}
                />
              )}
            />
            <Controller
              name="keyOne"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  label="Слова отрицаительные"
                  placeholderTextColor="white"
                  mode="flat"
                  theme={inputStyle}
                  style={{...styles.input, marginBottom: 24}}
                  onChangeText={(value: string) => {
                    props.onChange(value);
                  }}
                />
              )}
            />
            <Controller
              name="keyOne"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  label="Каналы через запятую"
                  placeholderTextColor="white"
                  mode="flat"
                  theme={inputStyle}
                  style={{...styles.input, marginBottom: 24}}
                  onChangeText={(value: string) => {
                    props.onChange(value);
                  }}
                />
              )}
            />
            <Controller
              name="keyOne"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  label="Черный список каналов"
                  placeholderTextColor="white"
                  mode="flat"
                  theme={inputStyle}
                  style={{...styles.input, marginBottom: 24}}
                  onChangeText={(value: string) => {
                    props.onChange(value);
                  }}
                />
              )}
            />
          </S.Container>
        </ScrollKeyboardLayout>
      </GS.SafeAreaView>
    );
  },
);
export default CreateKeyScreenComponent;
