import React, {useCallback, useEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {DefaultTheme, HelperText, TextInput} from 'react-native-paper';
import {Image} from 'react-native';
//Styles
import S, {styles} from './forgot-screen.styled';
import GS from '../../styles';
//Props
import {SignInScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';

//Store
import {useStore} from '../../store';
import {observer} from 'mobx-react-lite';
import ScrollKeyboardLayout from '../../components/scroll-keyboard-layout';

const ForgotPassword: React.FC<SignInScreenProps> = observer(({navigation}) => {
  const store = useStore();
  const {
    control,
    setError,
    errors,
    formState: {isSubmitting},
  } = useForm({
    defaultValues: {
      code: '',
      login: '',
    },
  });

  useEffect(() => {
    store.common.setLoading(isSubmitting);
    return () => {
      store.common.setLoading(false);
    };
  }, [isSubmitting, store.common]);

  const inputStyle = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: store.common.defaultTheme.primary,
      text: store.common.defaultTheme.textColor,
      placeholder: store.common.defaultTheme.primary,
    },
  };

  const [code, setCode] = useState<string>('');
  const [login, setLogin] = useState<string>('');

  useEffect(() => {
    if (store.auth.code) {
      if (code === store.auth.code) {
        navigation.navigate('ChangePas', {login: login});
      }
    }
  }, [code, login, navigation, store.auth.code]);

  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <ScrollKeyboardLayout>
        <S.Heading style={{color: store.common.defaultTheme.textColor}}>
          Восстановление пароля
        </S.Heading>
        <S.Container>
          <Controller
            name="login"
            control={control}
            render={(props) => {
              return (
                <>
                  <TextInput
                    {...props}
                    textContentType="telephoneNumber"
                    dataDetectorTypes="phoneNumber"
                    keyboardType="phone-pad"
                    label="Телефон"
                    defaultValue={login}
                    placeholderTextColor="white"
                    error={!!errors.login}
                    mode="flat"
                    right={
                      /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/.test(
                        props.value,
                      ) && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      props.onChange(value);
                      setLogin(value);
                    }}
                  />
                </>
              );
            }}
          />
          <HelperText type="error" visible={!!errors.login}>
            {errors.login && errors.login.message}
          </HelperText>
          {store.auth.code && (
            <>
              <Controller
                name="pas"
                control={control}
                render={(props) => {
                  return (
                    <>
                      <TextInput
                        {...props}
                        defaultValue={code}
                        label="code"
                        mode="flat"
                        textContentType="telephoneNumber"
                        dataDetectorTypes="phoneNumber"
                        keyboardType="phone-pad"
                        theme={inputStyle}
                        style={{
                          ...styles.input,
                          backgroundColor: store.common.defaultTheme.inputColor,
                        }}
                        right={
                          props.value === store.auth.code && (
                            <TextInput.Icon
                              name={() => (
                                <Image
                                  source={require('../../assets/Vector.png')}
                                />
                              )}
                            />
                          )
                        }
                        error={
                          props.value?.length > 0 &&
                          props.value !== store.auth.code
                        }
                        onChangeText={(value: string) => {
                          props.onChange(value);
                          setCode(value);
                        }}
                      />
                    </>
                  );
                }}
              />
              <HelperText
                type="error"
                visible={code?.length > 0 && code !== store.auth.code}>
                Неверный код
              </HelperText>
            </>
          )}
          <S.SignUpText style={{color: store.common.defaultTheme.singUpText}}>
            На ваш телефон будет отправлен код для восстановления пароля
          </S.SignUpText>
        </S.Container>
      </ScrollKeyboardLayout>
      <S.SignInButton
        style={{backgroundColor: store.common.defaultTheme.authBtnColor}}
        onPress={async () => {
          if (
            !/^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/.test(
              login,
            )
          ) {
            setError('login', {message: 'Заполните поле'});
          }
          await store.auth.newPas();
        }}
        disabled={isSubmitting}>
        <S.SignUpText style={styles.white}>Отправить</S.SignUpText>
      </S.SignInButton>
    </GS.SafeAreaView>
  );
});

export default ForgotPassword;
