// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import theme from '../../styles/theme';

const Container = styled.View`
  padding-left: 30px;
  padding-right: 30px;
`;
const InputGroup = styled.View`
  justify-content: center;
  align-items: center;
`;

const Heading = styled(Text)`
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  margin-top: 60px;
  margin-left: 31px;
  margin-bottom: 32px;
  width: 70%;
`;

const SignUpText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  /* or 137% */
  letter-spacing: -0.408px;
  text-align: center;

`;
const SignInButton = styled.TouchableOpacity`
  padding: 12px 17px;
  width: 193px;
  height: 45px;
  box-shadow: 0 4px 32px rgba(7, 8, 14, 0.05);
  border-radius: 8px;
  margin-left: auto;
  margin-right: 30px;
`;
export const styles = StyleSheet.create({
  input: {
    color: theme.colors.brandWhite,
    borderRadius: 4,
    height: 46,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 4,
  },
  orange: {
    color: '#F79827',
  },
  white: {
    color: '#ffffff',
  },
});

export default {
  Heading,
  Container,
  InputGroup,
  SignUpText,
  SignInButton,
};
