// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import theme from '../../styles/theme';

const ImageContainer = styled.View`
  align-items: center;
  justify-content: center;
  margin-bottom: 24px;
`;

const Container = styled.View`
  padding-left: 30px;
  padding-right: 30px;
`;
const InputGroup = styled.View`
  justify-content: center;
  align-items: center;
`;
const SignUp = styled.View`
  justify-content: center;
  align-items: center;
`;
const Heading = styled(Text)`
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  text-align: center;
  margin-bottom: 32px;
`;
const Forgot = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  margin-bottom: 5px;
`;
const SignUpText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.408px;
  text-align: center;
`;
const SignInButton = styled.TouchableOpacity`
  padding: 12px 17px;
  width: 193px;
  height: 45px;
  box-shadow: 0 4px 32px rgba(7, 8, 14, 0.05);
  border-radius: 8px;
  margin-left: auto;
  margin-right: auto;
`;
export const styles = StyleSheet.create({
  input: {
    color: theme.colors.brandWhite,
    borderRadius: 4,
    height: 46,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 4,
  },
  orange: {
    color: '#F79827',
  },
  white: {
    color: '#ffffff',
  },
});

export default {
  Heading,
  Container,
  InputGroup,
  ImageContainer,
  Forgot,
  SignUp,
  SignUpText,
  SignInButton,
};
