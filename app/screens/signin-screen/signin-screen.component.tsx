import React, {useCallback, useEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {DefaultTheme, HelperText, TextInput} from 'react-native-paper';
import {Image, TouchableOpacity} from 'react-native';
//Styles
import S, {styles} from './signin-screen.styled';
import GS from '../../styles';
//Props
import {SignInScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';

//Store
import {useStore} from '../../store';
import {observer} from 'mobx-react-lite';
import ScrollKeyboardLayout from '../../components/scroll-keyboard-layout';

const SignInScreen: React.FC<SignInScreenProps> = observer(({navigation}) => {
  const store = useStore();
  const [sendCode, setSendCode] = useState<boolean>(false);
  const {
    control,
    handleSubmit,
    formState: {isSubmitting},
  } = useForm({
    defaultValues: {
      login: '',
      code: '',
    },
  });

  useEffect(() => {
    store.common.setLoading(isSubmitting);
    return () => {
      store.common.setLoading(false);
    };
  }, [isSubmitting, store.common]);

  const inputStyle = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: store.common.defaultTheme.primary,
      text: store.common.defaultTheme.textColor,
      placeholder: store.common.defaultTheme.primary,
    },
  };

  const goSignUp = useCallback(() => {
    navigation.navigate('SignUp');
  }, [navigation]);

  useEffect(() => {
    if ((store.auth.newUser || store.auth.newUser === 'true') && sendCode) {
      goSignUp();
    }
  }, [goSignUp, sendCode, store.auth.auth, store.auth.newUser]);

  const onSubmit = handleSubmit(async (data) => {
    if (store.auth.auth?.result === true) {
      const formData = new FormData();
      formData.append('phone', data.login);
      formData.append('code', data.code);
      await store.auth.singIn(formData);
      setSendCode(true);
    } else if (
      store.auth.auth?.result === false ||
      typeof store.auth.auth?.result === 'string'
    ) {
      const formData = new FormData();
      formData.append('phone', data.login);
      await store.auth.newCode(formData);
    } else {
      const formData = new FormData();
      formData.append('phone', data.login);
      await store.auth.newCode(formData);
    }
  });

  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <ScrollKeyboardLayout>
        <S.Heading style={{color: store.common.defaultTheme.textColor}}>
          Авторизация/Регистрация
        </S.Heading>
        <S.ImageContainer>
          <Image
            source={require('../../assets/Image.png')}
            style={{width: '80%', resizeMode: 'contain'}}
          />
        </S.ImageContainer>
        <S.Container style={{flex: 1}}>
          <Controller
            name="login"
            control={control}
            render={(props) => {
              return (
                <>
                  <S.Forgot
                    style={{color: store.common.defaultTheme.textAuthColor}}>
                    На указанный номер будет отправлен код
                  </S.Forgot>
                  <TextInput
                    {...props}
                    textContentType="telephoneNumber"
                    dataDetectorTypes="phoneNumber"
                    keyboardType="phone-pad"
                    label="Телефон"
                    placeholderTextColor="white"
                    error={typeof store.auth.auth?.result === 'string'}
                    mode="outlined"
                    right={
                      /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/.test(
                        props.value,
                      ) && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      props.onChange(value);
                    }}
                  />
                </>
              );
            }}
          />
          <HelperText
            type="error"
            visible={typeof store.auth.auth?.result === 'string'}>
            {store.auth.auth?.result}
          </HelperText>
          {store.auth.auth?.result &&
            typeof store.auth.auth?.result !== 'string' && (
              <Controller
                name="code"
                control={control}
                render={(props) => {
                  return (
                    <>
                      <TextInput
                        {...props}
                        textContentType="telephoneNumber"
                        dataDetectorTypes="phoneNumber"
                        keyboardType="phone-pad"
                        label="Код"
                        placeholderTextColor="white"
                        error={!!store.auth.user?.error}
                        mode="outlined"
                        right={
                          props.value?.length > 0 && (
                            <TextInput.Icon
                              name={() => (
                                <Image
                                  source={require('../../assets/Vector.png')}
                                />
                              )}
                            />
                          )
                        }
                        theme={inputStyle}
                        style={{
                          ...styles.input,
                          backgroundColor: store.common.defaultTheme.inputColor,
                          marginBottom: 5,
                        }}
                        onChangeText={(value: string) => {
                          props.onChange(value);
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          store.auth.setAuth(undefined);
                        }}>
                        <S.Forgot
                          style={{
                            color: store.common.defaultTheme.textAuthColor,
                          }}>
                          Указать другой номер телефона
                        </S.Forgot>
                      </TouchableOpacity>
                    </>
                  );
                }}
              />
            )}
          {/*<S.SignUp>*/}
          {/*  <S.SignUpText style={{color: store.common.defaultTheme.singUpText}}>*/}
          {/*    Вы не зарегистрированы?*/}
          {/*  </S.SignUpText>*/}
          {/*  <TouchableOpacity onPress={goSignUp}>*/}
          {/*    <S.SignUpText*/}
          {/*      style={{color: store.common.defaultTheme.textAuthColor}}>*/}
          {/*      Создать аккаунт*/}
          {/*    </S.SignUpText>*/}
          {/*  </TouchableOpacity>*/}
          {/*</S.SignUp>*/}
        </S.Container>
      </ScrollKeyboardLayout>
      <S.SignInButton
        onPress={onSubmit}
        disabled={isSubmitting}
        style={{backgroundColor: store.common.defaultTheme.authBtnColor}}>
        <S.SignUpText style={styles.white}>
          {store.auth.auth?.result === true
            ? 'Войти в приложение'
            : store.auth.auth?.result === false
            ? 'Отправить код повторно'
            : 'Отправить код'}
        </S.SignUpText>
      </S.SignInButton>
    </GS.SafeAreaView>
  );
});

export default SignInScreen;
