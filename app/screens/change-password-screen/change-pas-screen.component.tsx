import React, {useCallback, useEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {DefaultTheme, HelperText, TextInput} from 'react-native-paper';
import {Image} from 'react-native';
//Styles
import S, {styles} from './change-pas-screen.styled';
import GS from '../../styles';
//Props
import {SignInScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';

//Store
import {useStore} from '../../store';
import {observer} from 'mobx-react-lite';
import ScrollKeyboardLayout from '../../components/scroll-keyboard-layout';

const ChangePasScreen: React.FC<SignInScreenProps> = observer(
  ({navigation, route}) => {
    const {login} = route.params ?? {login: ''};
    const store = useStore();
    const {
      setError,
      errors,
      control,
      handleSubmit,
      formState: {isSubmitting},
    } = useForm({
      defaultValues: {
        pas: '',
        pasConfirmation: '',
      },
    });

    useEffect(() => {
      store.common.setLoading(isSubmitting);
      return () => {
        store.common.setLoading(false);
      };
    }, [isSubmitting, store.common]);

    const inputStyle = {
      ...DefaultTheme,
      colors: {
        ...DefaultTheme.colors,
        primary: store.common.defaultTheme.primary,
        text: store.common.defaultTheme.textColor,
        placeholder: store.common.defaultTheme.primary,
      },
    };

    const [pas, setPas] = useState<string>('');
    const [pasConfirmation, setPasConfirmation] = useState<string>('');
    const onSubmit = handleSubmit(async () => {
      if (pas.length < 6) {
        setError('pas', {
          message: 'Пароль должен содеражить минимум 6 симлвов',
        });
      } else if (pasConfirmation !== pas && pasConfirmation.length > 0) {
        setError('pasConfirmation', {message: 'Пароли не совпадают'});
      } else {
        try {
          await store.auth.changePas(pas, login);
          navigation.navigate('SignIn');
        } catch (e) {}
      }
    });
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <ScrollKeyboardLayout>
          <S.Heading style={{color: store.common.defaultTheme.textColor}}>
            Придумайте новый пароль
          </S.Heading>
          <S.Container>
            <Controller
              name="pas"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  defaultValue={pas}
                  label="Пароль"
                  mode="flat"
                  right={
                    pas.length >= 6 && (
                      <TextInput.Icon
                        name={() => (
                          <Image source={require('../../assets/Vector.png')} />
                        )}
                      />
                    )
                  }
                  error={!!errors.pas}
                  secureTextEntry
                  theme={inputStyle}
                  style={{
                    ...styles.input,
                    backgroundColor: store.common.defaultTheme.inputColor,
                  }}
                  onChangeText={(value: string) => {
                    setPas(value);
                  }}
                />
              )}
            />
            <HelperText type="error" visible={!!errors.pas}>
              {errors.pas && errors.pas.message}
            </HelperText>
            <Controller
              name="pasConfirmation"
              control={control}
              render={(props) => (
                <TextInput
                  {...props}
                  defaultValue={pasConfirmation}
                  label="Подтверждение пароля"
                  mode="flat"
                  right={
                    pasConfirmation.length > 0 &&
                    pasConfirmation === pas && (
                      <TextInput.Icon
                        name={() => (
                          <Image source={require('../../assets/Vector.png')} />
                        )}
                      />
                    )
                  }
                  error={!!errors.pasConfirmation}
                  secureTextEntry
                  theme={inputStyle}
                  style={{
                    ...styles.input,
                    backgroundColor: store.common.defaultTheme.inputColor,
                  }}
                  onChangeText={(value: string) => {
                    setPasConfirmation(value);
                  }}
                />
              )}
            />
            <HelperText type="error" visible={!!errors.pasConfirmation}>
              {errors.pasConfirmation && errors.pasConfirmation.message}
            </HelperText>
          </S.Container>
        </ScrollKeyboardLayout>
        <S.SignInButton
          onPress={onSubmit}
          disabled={isSubmitting}
          style={{backgroundColor: store.common.defaultTheme.authBtnColor}}>
          <S.SignUpText style={styles.white}>Отправить</S.SignUpText>
        </S.SignInButton>
      </GS.SafeAreaView>
    );
  },
);

export default ChangePasScreen;
