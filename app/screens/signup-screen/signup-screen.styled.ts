// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet, TextProps} from 'react-native';
import {Text, TextInput} from 'react-native-paper';
import theme from '../../styles/theme';

const ImageContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  margin-bottom: 24px;
`;

const Container = styled.View`
  flex: 1;
  padding-left: 30px;
  padding-right: 30px;
`;
const InputGroup = styled.View`
  justify-content: center;
  align-items: center;
`;

const Heading = styled(Text)`
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  margin-top: 32px;
  margin-left: 30px;
  margin-bottom: 32px;
`;

const SignUpText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.408px;
`;
const SignUpButton = styled.TouchableOpacity`
  text-align: center;
  justify-content: center;
  align-items: center;
  width: 193px;
  height: 45px;
  box-shadow: 0 4px 32px rgba(7, 8, 14, 0.05);
  border-radius: 8px;
  margin-left: auto;
  margin-right: 30px;
`;
const SectionContainer = styled.View`
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  margin-right: 28px;
`;
const SectionText = styled.TouchableOpacity`
  background: ${({isActive}) => (isActive ? '#F79827' : '#979797')};
  border-radius: ${({isActive}) => (isActive ? '2.5px' : '3.5px')};
  width: ${({isActive}) => (isActive ? '15px' : '5px')};
  height: 5px;
  margin-left: 5px;
`;
const SectionKeyContainer = styled.View`
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  margin-bottom: 32px;
`;
const InputContainer = styled.View`
  flex-direction: row;
  margin-bottom: 24px;
`;
const Input = styled.TextInput`
  background: #2d2d2d;
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.05);
  border-radius: 8px;
  height: 36px;
  padding-left: 18.43px;
  width: 95%;
  flex-direction: row;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;
const AddKey = styled.TouchableOpacity`
  width: 33px;
  height: 36px;
  justify-content: center;
  align-items: center;
  position: absolute;
  margin-left: 90%;
  border-radius: 50px;
`;
const KeyContainer = styled.View`
  background: #2d2d2d;
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.05);
  border-radius: 8px;
  height: 36px;
  margin-bottom: 24px;
  padding-left: 18.43px;
  flex-direction: row;
  align-items: center;
`;
const KeyText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  display: flex;
  align-items: center;
  letter-spacing: -0.32px;
`;

const DeleteKey = styled.TouchableOpacity`
  margin-left: 95%;
  position: absolute;
`;
interface SectionTextProps extends TextProps {
  isActive: boolean;
}
const SectionKey = styled(Text)<SectionTextProps>`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  border-bottom-width: ${({isActive}) => (isActive ? '1px' : '0px')};
  border-bottom-color: #f79827;
  padding-bottom: 4px;
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
`;
export const styles = StyleSheet.create({
  input: {
    color: theme.colors.brandWhite,
    borderRadius: 4,
    height: 46,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 4,
  },
  orange: {
    color: '#F79827',
  },
  white: {
    color: '#ffffff',
  },
});

export default {
  Heading,
  Container,
  InputGroup,
  ImageContainer,
  SignUpButton,
  SignUpText,
  SectionText,
  SectionContainer,
  SectionKeyContainer,
  SectionKey,
  AddKey,
  DeleteKey,
  KeyText,
  KeyContainer,
  Input,
  InputContainer,
};
