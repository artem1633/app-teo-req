import React, {useCallback, useEffect, useLayoutEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {DefaultTheme, HelperText, TextInput} from 'react-native-paper';
import {
  Alert,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  View,
} from 'react-native';
//Styles
import S, {styles} from './signup-screen.styled';
import GS from '../../styles';
//Props
import {SignUpScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';
//Components
import ScrollKeyboardLayout from '../../components/scroll-keyboard-layout';
import {observer} from 'mobx-react-lite';
import {useStore} from '../../store';
import Icon from '../../components/icon/icon.component';

// @ts-ignore
const SignUpScreen: React.FC<SignUpScreenProps> = observer(({navigation}) => {
  const {control, handleSubmit, errors, setError} = useForm({
    defaultValues: {
      name: '',
      phone: '',
      email: '',
      pas: '',
      pasConfirmation: '',
      promo: '',
    },
  });

  const [step, setStep] = useState<string>('one');
  const store = useStore();
  const inputStyle = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: store.common.defaultTheme.primary,
      text: store.common.defaultTheme.textColor,
      placeholder: store.common.defaultTheme.primary,
    },
  };
  const navigateBtn = useCallback(() => {
    switch (step) {
      case 'one':
        return navigation.goBack();
      case 'two':
        return setStep('one');
      case 'three':
        return setStep('two');
      case 'four':
        return setStep('three');
    }
  }, [navigation, step]);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity style={{marginLeft: 25}} onPress={navigateBtn}>
          <Image
            source={
              store.common.defaultTheme.background === 'black'
                ? require('../../assets/BackBtn.png')
                : require('../../assets/BlackBack.png')
            }
          />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <S.SectionContainer>
          <S.SectionText isActive={step === 'one'} />
          <S.SectionText isActive={step === 'two'} />
          <S.SectionText isActive={step === 'three' || step === 'four'} />
        </S.SectionContainer>
      ),
    });
  }, [navigateBtn, navigation, step, store.common.defaultTheme.background]);

  const [name, setName] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [pas, setPas] = useState<string>('');
  const [pasConfirmation, setPasConfirmation] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const onSubmit = handleSubmit(async () => {
    if (pas.length < 6) {
      setError('pas', {message: 'Пароль должен содеражить минимум 6 симлвов'});
    } else if (pasConfirmation !== pas && pasConfirmation.length > 0) {
      setError('pasConfirmation', {message: 'Пароли не совпадают'});
    } else {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('pas', pas);
      formData.append('email', email);
      formData.append('phone', phone);
      await store.auth.singUp(formData);
      if (store.auth.signUpError?.data.error) {
        setStep('one');
      } else {
        setStep('three');
      }
    }
  });

  useEffect(() => {
    if (store.auth.newUser === 'true' || store.auth.newUser) {
      setStep('three');
    }
  }, [store.auth.newUser]);

  const stepOne = useCallback(() => {
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <View style={{marginTop: 32, flex: 1}}>
          <ScrollKeyboardLayout>
            <S.Heading style={{color: store.common.defaultTheme.textColor}}>
              Регистрация
            </S.Heading>
            <S.ImageContainer>
              <Image
                source={require('../../assets/Image.png')}
                style={{width: '80%', resizeMode: 'contain'}}
              />
            </S.ImageContainer>
            <S.Container>
              <Controller
                name="name"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    defaultValue={name}
                    label="Имя и фамилия"
                    placeholderTextColor="white"
                    mode="flat"
                    right={
                      !/^\s*$/.test(name) && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    error={!!errors.name}
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      setName(value);
                    }}
                  />
                )}
              />
              <HelperText type="error" visible={!!errors.name}>
                {errors.name && errors.name.message}
              </HelperText>
              <Controller
                name="phone"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    defaultValue={phone}
                    label="Номер телефона"
                    mode="flat"
                    textContentType="telephoneNumber"
                    dataDetectorTypes="phoneNumber"
                    keyboardType="phone-pad"
                    right={
                      !/^\s*$/.test(phone) && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    error={!!errors.phone || store.auth.signUpError?.data.error}
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      setPhone(value);
                    }}
                  />
                )}
              />
              <HelperText
                type="error"
                visible={!!errors.phone || store.auth.signUpError?.data.error}>
                {(errors.phone && errors.phone.message) ||
                  store.auth.signUpError?.data.error}
              </HelperText>
              <Controller
                name="email"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    defaultValue={email}
                    label="E-mail"
                    mode="flat"
                    theme={inputStyle}
                    right={
                      /\S+@\S+\.\S+/.test(email) && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    error={!!errors.email || store.auth.signUpError?.data.error}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      setEmail(value);
                    }}
                  />
                )}
              />
              <HelperText
                type="error"
                visible={!!errors.email || store.auth.signUpError?.data.error}>
                {(errors.email && errors.email.message) ||
                  store.auth.signUpError?.data.error}
              </HelperText>
            </S.Container>
          </ScrollKeyboardLayout>
        </View>
        <S.SignUpButton
          style={{backgroundColor: store.common.defaultTheme.authBtnColor}}
          onPress={() => {
            if (/^\s*$/.test(name)) {
              setError('name', {message: 'Заполните поле'});
            } else if (/^\s*$/.test(phone)) {
              setError('phone', {message: 'Заполните поле'});
            } else if (!/\S+@\S+\.\S+/.test(email)) {
              setError('email', {message: 'Введите email'});
            } else {
              setStep('two');
            }
          }}>
          <S.SignUpText style={styles.white}>Продолжить</S.SignUpText>
        </S.SignUpButton>
      </GS.SafeAreaView>
    );
  }, [
    control,
    email,
    errors.email,
    errors.name,
    errors.phone,
    inputStyle,
    name,
    phone,
    setError,
    store.auth.signUpError,
    store.common.defaultTheme.authBtnColor,
    store.common.defaultTheme.background,
    store.common.defaultTheme.inputColor,
    store.common.defaultTheme.textColor,
  ]);

  const stepTwo = useCallback(() => {
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <View style={{marginTop: 32, flex: 1}}>
          <ScrollKeyboardLayout>
            <S.Heading style={{color: store.common.defaultTheme.textColor}}>
              Регистрация
            </S.Heading>
            <S.ImageContainer>
              <Image
                source={require('../../assets/Image.png')}
                style={{width: '80%', resizeMode: 'contain'}}
              />
            </S.ImageContainer>
            <S.Container>
              <Controller
                name="pas"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    defaultValue={pas}
                    label="Пароль"
                    mode="flat"
                    right={
                      pas.length >= 6 && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    error={!!errors.pas}
                    secureTextEntry
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      setPas(value);
                    }}
                  />
                )}
              />
              <HelperText type="error" visible={!!errors.pas}>
                {errors.pas && errors.pas.message}
              </HelperText>
              <Controller
                name="pasConfirmation"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    defaultValue={pasConfirmation}
                    label="Подтверждение пароля"
                    mode="flat"
                    right={
                      pasConfirmation.length > 0 &&
                      pasConfirmation === pas && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    error={!!errors.pasConfirmation}
                    secureTextEntry
                    theme={inputStyle}
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      setPasConfirmation(value);
                    }}
                  />
                )}
              />
              <HelperText type="error" visible={!!errors.pasConfirmation}>
                {errors.pasConfirmation && errors.pasConfirmation.message}
              </HelperText>
              <Controller
                name="promo"
                control={control}
                render={(props) => (
                  <TextInput
                    {...props}
                    label="Промокод"
                    mode="flat"
                    theme={inputStyle}
                    right={
                      props.value.length > 0 && (
                        <TextInput.Icon
                          name={() => (
                            <Image
                              source={require('../../assets/Vector.png')}
                            />
                          )}
                        />
                      )
                    }
                    style={{
                      ...styles.input,
                      backgroundColor: store.common.defaultTheme.inputColor,
                    }}
                    onChangeText={(value: string) => {
                      props.onChange(value);
                    }}
                  />
                )}
              />
            </S.Container>
          </ScrollKeyboardLayout>
        </View>
        <S.SignUpButton
          onPress={onSubmit}
          style={{backgroundColor: store.common.defaultTheme.authBtnColor}}>
          <S.SignUpText style={styles.white}>Продолжить</S.SignUpText>
        </S.SignUpButton>
      </GS.SafeAreaView>
    );
  }, [
    control,
    errors.pas,
    errors.pasConfirmation,
    inputStyle,
    onSubmit,
    pas,
    pasConfirmation,
    store.common.defaultTheme.authBtnColor,
    store.common.defaultTheme.background,
    store.common.defaultTheme.inputColor,
    store.common.defaultTheme.textColor,
  ]);
  const [section, setSection] = useState<string>('keyOne');
  const [value, onChangeText] = useState<string>('');
  const [key, setKey] = useState<any>([]);
  const [minus, setMinus] = useState<any>([]);
  const onChange = (event: {
    nativeEvent: {eventCount: any; target: any; text: any};
  }) => {
    const {text} = event.nativeEvent;
    onChangeText(text);
  };
  const addKeyOne = useCallback(async () => {
    key.push({
      id: (Math.random().toString(16) + '00000000000000000').slice(2, 12 + 2),
      keyId: (Math.random().toString(16) + '00000000000000000').slice(
        2,
        12 + 2,
      ),
      value: value,
    });
    await store.common.addSignUpKeyOne(key);
  }, [key, store.common, value]);
  const addKeyMinus = useCallback(async () => {
    minus.push({
      id: (Math.random().toString(16) + '00000000000000000').slice(2, 12 + 2),
      keyId: (Math.random().toString(16) + '00000000000000000').slice(
        2,
        12 + 2,
      ),
      value: value,
    });
    await store.common.addSignUpKeyMinus(minus);
  }, [minus, store.common, value]);
  const deleteKeyOne = useCallback(
    async (id: number) => {
      const updateArray = key.filter((item: {id: number}) => id !== item.id);
      setKey(updateArray);
      await store.common.addSignUpKeyMinus(key);
    },
    [key, store.common],
  );
  const deleteKeyMinus = useCallback(
    async (id: number) => {
      const updateArray = minus.filter((item: {id: number}) => id !== item.id);
      setMinus(updateArray);
      await store.common.addSignUpKeyMinus(minus);
    },
    [minus, store.common],
  );
  const addKey = useCallback(async () => {
    if (/^\s*$/.test(value)) {
      Alert.alert('Заполните поле');
    } else {
      section === 'keyOne' ? await addKeyOne() : await addKeyMinus();
      onChangeText('');
    }
  }, [addKeyMinus, addKeyOne, section, value]);
  const deleteKey = useCallback(
    async (id: number) => {
      section === 'keyOne' ? await deleteKeyOne(id) : await deleteKeyMinus(id);
    },
    [deleteKeyMinus, deleteKeyOne, section],
  );
  const renderKeyContent = useCallback(() => {
    const keyOne = key
      ?.slice()
      .reverse()
      .map((item: any) => {
        return (
          <S.KeyContainer
            key={item.id}
            style={{
              backgroundColor: store.common.defaultTheme.inputColor,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,

              elevation: 2,
            }}>
            <S.KeyText style={{color: store.common.defaultTheme.textColor}}>
              {item.value}
            </S.KeyText>
            <S.DeleteKey onPress={() => deleteKey(item.id)}>
              <Image
                source={require('../../assets/Shape.png')}
                style={{resizeMode: 'contain'}}
              />
            </S.DeleteKey>
          </S.KeyContainer>
        );
      });
    const keyMinus = minus
      ?.slice()
      .reverse()
      .map((item: any) => {
        return (
          <S.KeyContainer
            key={item.id}
            style={{
              backgroundColor: store.common.defaultTheme.inputColor,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,

              elevation: 2,
            }}>
            <S.KeyText style={{color: store.common.defaultTheme.textColor}}>
              {item.value}
            </S.KeyText>
            <S.DeleteKey onPress={() => deleteKey(item.id)}>
              <Image
                source={require('../../assets/Shape.png')}
                style={{resizeMode: 'contain'}}
              />
            </S.DeleteKey>
          </S.KeyContainer>
        );
      });
    switch (section) {
      case 'keyOne':
        return keyOne;
      case 'keyMinus':
        return keyMinus;
      default:
        return keyOne;
    }
  }, [
    deleteKey,
    key,
    minus,
    section,
    store.common.defaultTheme.inputColor,
    store.common.defaultTheme.textColor,
  ]);
  const stepThree = useCallback(() => {
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <View style={{marginTop: 32, flex: 1}}>
          <S.Heading style={{color: store.common.defaultTheme.textColor}}>
            Добавьте ключевые слова для поиска
          </S.Heading>
          <S.Container>
            <S.SectionKeyContainer>
              <S.SectionKey
                style={{
                  color: store.common.defaultTheme.textColor,
                  paddingVertical: 4,
                }}
                isActive={section === 'keyOne'}
                onPress={() => setSection('keyOne')}>
                Положительные
              </S.SectionKey>
              <S.SectionKey
                style={{
                  color: store.common.defaultTheme.textColor,
                  paddingVertical: 4,
                }}
                isActive={section === 'keyMinus'}
                onPress={() => setSection('keyMinus')}>
                Отрицательные
              </S.SectionKey>
            </S.SectionKeyContainer>
            <S.InputContainer>
              <S.Input
                placeholder={'Введите название ключевика'}
                placeholderTextColor={store.common.defaultTheme.placeholder}
                style={{
                  backgroundColor: store.common.defaultTheme.inputColor,
                  color: store.common.defaultTheme.textColor,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 4,
                  },
                  shadowOpacity: 0.3,
                  shadowRadius: 4.65,

                  elevation: 4,
                }}
                value={value}
                onChange={onChange}
              />
              <S.AddKey
                onPress={addKey}
                style={{
                  backgroundColor: store.common.defaultTheme.authBtnColor,
                  elevation: 4,
                }}>
                <Icon name={'arrow-right'} size={15} color={'white'} />
              </S.AddKey>
            </S.InputContainer>
            <ScrollKeyboardLayout>{renderKeyContent()}</ScrollKeyboardLayout>
          </S.Container>
        </View>
        <S.SignUpButton
          onPress={() => setStep('four')}
          style={{backgroundColor: store.common.defaultTheme.authBtnColor}}>
          <S.SignUpText style={styles.white}>Продолжить</S.SignUpText>
        </S.SignUpButton>
      </GS.SafeAreaView>
    );
  }, [
    addKey,
    renderKeyContent,
    section,
    store.common.defaultTheme.authBtnColor,
    store.common.defaultTheme.background,
    store.common.defaultTheme.inputColor,
    store.common.defaultTheme.placeholder,
    store.common.defaultTheme.textColor,
    value,
  ]);
  const stepFour = useCallback(() => {
    return (
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <View style={{marginTop: 32, flex: 1}}>
          <ScrollKeyboardLayout>
            <S.Heading
              style={{
                marginBottom: 60,
                color: store.common.defaultTheme.textColor,
              }}>
              Благодарим за регистрацию!
            </S.Heading>
            <S.ImageContainer style={{marginBottom: 40}}>
              <Image source={require('../../assets/SignUp.png')} />
            </S.ImageContainer>
            <S.SignUpText
              style={{
                textAlign: 'center',
                color: store.common.defaultTheme.textColor,
              }}>
              Заявки по вашим запросам будут доступны на главном экране
            </S.SignUpText>
          </ScrollKeyboardLayout>
        </View>
        <S.SignUpButton
          onPress={async () => {
            await store.auth.singInWithToken();
            store.auth.setNewUser('false');
          }}
          style={{backgroundColor: store.common.defaultTheme.authBtnColor}}>
          <S.SignUpText style={styles.white}>Продолжить</S.SignUpText>
        </S.SignUpButton>
      </GS.SafeAreaView>
    );
  }, [
    store.auth,
    store.common.defaultTheme.authBtnColor,
    store.common.defaultTheme.background,
    store.common.defaultTheme.textColor,
  ]);
  const renderContent = useCallback(() => {
    switch (step) {
      case 'one':
        return stepOne();
      case 'two':
        return stepTwo();
      case 'three':
        return stepThree();
      case 'four':
        return stepFour();
    }
  }, [step, stepFour, stepOne, stepThree, stepTwo]);

  return renderContent();
});

export default SignUpScreen;
