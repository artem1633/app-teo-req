import React from 'react';
//Styles
import GS from '../../styles';
import S from './feedback-screen.styled';
import {observer} from 'mobx-react-lite';
import {useStore} from '../../store';
interface FeedbackScreenProps {}
const FeedbackScreen: React.FC<FeedbackScreenProps> = observer(() => {
  const store = useStore();
  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <S.Container>
        <S.Heading style={{color: store.common.defaultTheme.textColor}}>
          TEO-Request
        </S.Heading>
        <S.InfoText style={{color: store.common.defaultTheme.textColor}}>
          Более 8 лет мы создаем облачные IT-решения для малого и среднего
          бизнеса. Наши основные клиенты – компании, которые не нашли готовую
          автоматизированную программу и хотят разработать crm-систему под свои
          потребности и задачи. Наша миссия – сделать бизнес-процессы проще и
          прозрачнее за счет автоматизации. За годы работы мы создали более 150
          индивидуальных решений, каждое из которых на 100% отвечает
          техническому заданию заказчика. Наша цель – сделать процесс интеграции
          наиболее быстрым, легким и эффективным за счет понятного функционала и
          постоянной технической поддержки.
        </S.InfoText>
        <S.QuestionText style={{color: store.common.defaultTheme.textColor}}>
          Есть вопросы? Свяжитесь с нами!
        </S.QuestionText>
        <S.ContactsText style={{color: store.common.defaultTheme.textColor}}>
          +7(961)220-66-59
        </S.ContactsText>
        <S.ContactsText style={{color: store.common.defaultTheme.textColor}}>
          info@vkrassilka.ru
        </S.ContactsText>
      </S.Container>
    </GS.SafeAreaView>
  );
});

export default FeedbackScreen;
