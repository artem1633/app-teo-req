// @ts-ignore
import styled from 'styled-components/native';
import {FlatList as FlatListLib, StyleSheet, TextProps} from 'react-native';
import {Text} from 'react-native-paper';

const Container = styled.View`
  padding-left: 31px;
  padding-right: 28px;
  padding-top: 60px;
`;
const Heading = styled(Text)`
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  margin-bottom: 19px;
`;

const InfoText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.408px;
  margin-bottom: 28px;
`;
const QuestionText = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.75px;
  width: 60%;
  margin-bottom: 10px;
`;
const ContactsText = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 17px;
  line-height: 22px;
  letter-spacing: -0.408px;
  margin-bottom: 8px;
`;
export const styles = StyleSheet.create({});

export default {
  Heading,
  Container,
  InfoText,
  QuestionText,
  ContactsText,
};
