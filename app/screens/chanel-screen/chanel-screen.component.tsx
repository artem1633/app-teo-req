import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Alert,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  Text,
  View,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
//Styles
import GS from '../../styles';
import S from './chanel-screen.styled';
//Store
import {useStore} from '../../store';
//Components
import Icon from '../../components/icon';
import {observer} from 'mobx-react-lite';
//Props
import {KeywordScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';
import DataStorage from '../../utils/data-storage';

const ChanelScreen: React.FC<KeywordScreenProps> = observer(() => {
  const store = useStore();
  const [section, setSection] = useState<string>('keyChanel');
  const [value, onChangeText] = useState<string>('');

  const onChange = (event: {
    nativeEvent: {eventCount: any; target: any; text: any};
  }) => {
    const {text} = event.nativeEvent;
    onChangeText(text);
  };
  const addKey = useCallback(async () => {
    if (/^\s*$/.test(value)) {
      Alert.alert('Заполните поле');
    } else {
      section === 'keyChanel'
        ? await store.common.addKeyChanel(value, store.common.base)
        : await store.common.addKeyChanelBlack(value);
      onChangeText('');
    }
  }, [section, store.common, value]);
  const deleteKey = useCallback(
    async (id: number) => {
      section === 'keyChanel'
        ? await store.common.deleteKeyChanel(id)
        : await store.common.deleteKeyChanelBlack(id);
    },
    [section, store.common],
  );
  const renderItem = useCallback(
    ({item, index}) => {
      return (
        <S.ChanelContainer
          style={{
            backgroundColor: store.common.defaultTheme.inputColor,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 4.65,

            elevation: 2,
          }}
          key={(Math.random().toString(16) + '00000000000000000').slice(
            2,
            12 + 2,
          )}>
          <S.ChanelText style={{color: store.common.defaultTheme.textColor}}>
            @{item.url}
          </S.ChanelText>
          <S.DeleteChanel onPress={() => deleteKey(index)}>
            <Image
              source={require('../../assets/Shape.png')}
              style={{resizeMode: 'contain'}}
            />
          </S.DeleteChanel>
        </S.ChanelContainer>
      );
    },
    [
      deleteKey,
      store.common.defaultTheme.inputColor,
      store.common.defaultTheme.textColor,
    ],
  );
  const keyExtractor = useCallback((item) => `${item.url}`, []);
  const listRef = useRef<FlatList>(null);
  const listOffset = useRef<number | null>(null);
  const handleScroll = (e: any) => {
    listOffset.current = e.nativeEvent.contentOffset.y;
  };
  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <S.Container>
          <S.Heading style={{color: store.common.defaultTheme.textColor}}>
            Мои каналы
          </S.Heading>
          <S.SectionContainer>
            <S.SectionText
              style={{
                color:
                  section === 'keyChanel'
                    ? store.common.defaultTheme.textColor
                    : '#818C99',
                paddingVertical: 4,
              }}
              isActive={section === 'keyChanel'}
              onPress={async () => {
                setSection('keyChanel');
                await store.common.getMyKey();
              }}>
              Белый список
            </S.SectionText>
            <S.SectionText
              style={{
                color:
                  section === 'keyChanelBlack'
                    ? store.common.defaultTheme.textColor
                    : '#818C99',
                paddingVertical: 4,
              }}
              isActive={section === 'keyChanelBlack'}
              onPress={async () => {
                setSection('keyChanelBlack');
                await store.common.getMyKey();
              }}>
              Чёрный список
            </S.SectionText>
          </S.SectionContainer>
          {section === 'keyChanel' && (
            <View
              style={{
                flex: 0,
                marginBottom: 32,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <CheckBox
                disabled={false}
                value={store.common.base}
                onValueChange={async (newValue) => {
                  await store.common.setBase(newValue);
                }}
                onCheckColor={store.common.defaultTheme.authBtnColor}
                tintColors={{
                  true: store.common.defaultTheme.authBtnColor,
                  false: store.common.defaultTheme.authBtnColor,
                }}
                tintColor={store.common.defaultTheme.authBtnColor}
                onFillColor={store.common.defaultTheme.authBtnColor}
              />
              <Text
                style={{
                  color: store.common.defaultTheme.textColor,
                  marginLeft: 13,
                }}>
                Поиск по общей базе
              </Text>
            </View>
          )}
          <S.InputContainer>
            <S.Input
              placeholder={'Введите название канала'}
              placeholderTextColor={store.common.defaultTheme.placeholder}
              style={{
                backgroundColor: store.common.defaultTheme.inputColor,
                color: store.common.defaultTheme.textColor,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.3,
                shadowRadius: 4.65,

                elevation: 4,
              }}
              value={value}
              onChange={onChange}
            />
            <S.AddChanel
              onPress={addKey}
              style={{
                backgroundColor: '#F79827',
                elevation: 4,
              }}>
              <Image
                source={require('../../assets/right--v1.png')}
                style={{resizeMode: 'contain', width: 30}}
              />
            </S.AddChanel>
          </S.InputContainer>
          {store.common.myKey.data.keyChanel ||
          store.common.myKey.data.keyChanelBlack ? (
            <S.FlatList
              ref={listRef}
              data={
                section === 'keyChanel'
                  ? store.common.myKey.data.keyChanel.slice().reverse()
                  : store.common.myKey.data.keyChanelBlack.slice().reverse()
              }
              keyExtractor={keyExtractor}
              onScroll={handleScroll}
              renderItem={renderItem}
            />
          ) : null}
        </S.Container>
      </KeyboardAvoidingView>
    </GS.SafeAreaView>
  );
});

export default ChanelScreen;
