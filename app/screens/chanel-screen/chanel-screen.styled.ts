// @ts-ignore
import styled from 'styled-components/native';
import {FlatList as FlatListLib, StyleSheet, TextProps} from 'react-native';
import {Text} from 'react-native-paper';

const Container = styled.View`
  padding-left: 31px;
  padding-right: 28px;
  padding-top: 60px;
`;
const Heading = styled(Text)`
  font-weight: bold;
  font-size: 28px;
  line-height: 33px;
  letter-spacing: 0.87px;
  margin-bottom: 29.5px;
`;
const SectionContainer = styled.View`
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  margin-bottom: 32px;
`;
const InputContainer = styled.View`
  flex-direction: row;
  margin-bottom: 24px;
`;
const Input = styled.TextInput`
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.05);
  border-radius: 8px;
  height: 36px;
  padding-left: 18.43px;
  width: 94%;
  flex-direction: row;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;
const AddChanel = styled.TouchableOpacity`
  width: 33px;
  height: 36px;
  justify-content: center;
  align-items: center;
  position: absolute;
  margin-left: 90%;
  border-radius: 50px;
`;
const ChanelContainer = styled.View`
  background: #2d2d2d;
  box-shadow: 0 1px 8px rgba(0, 0, 0, 0.05);
  border-radius: 8px;
  height: 36px;
  margin-bottom: 24px;
  padding-left: 18.43px;
  flex-direction: row;
  align-items: center;
  width: 99%;
`;
const ChanelText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  display: flex;
  align-items: center;
  letter-spacing: -0.32px;
`;
const DeleteChanel = styled.TouchableOpacity`
  margin-left: 95%;
  position: absolute;
`;
interface SectionTextProps extends TextProps {
  isActive: boolean;
}
const SectionText = styled(Text)<SectionTextProps>`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  border-bottom-width: ${({isActive}) => (isActive ? '1px' : '0px')};
  border-bottom-color: #f79827;
  padding-bottom: 4px;
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
`;
const FlatList = styled(FlatListLib as new () => FlatListLib)`
  height: 69%;
`;
export const styles = StyleSheet.create({});

export default {
  Heading,
  Container,
  SectionContainer,
  SectionText,
  ChanelContainer,
  ChanelText,
  DeleteChanel,
  Input,
  InputContainer,
  AddChanel,
  FlatList,
};
