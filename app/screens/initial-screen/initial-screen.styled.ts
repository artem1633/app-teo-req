// @ts-ignore
import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';

const BottomContainer = styled.View`
  justify-content: center;
  align-items: center;
  bottom: 41px;
`;

const BottomText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  color: white;
`;

export const styles = StyleSheet.create({
  orange: {
    color: '#F79827',
  },
  tooltip: {
    position: 'absolute',
    top: '53%',
    left: '53.75%',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 66,
  },
});

export default {
  BottomText,
  BottomContainer,
};
