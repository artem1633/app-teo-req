import React, {useEffect, useState} from 'react';
import {ImageBackground, Text} from 'react-native';
//Image
// @ts-ignore
import Logo from '../../assets/AppLogo.png';
// @ts-ignore
import WhiteLogo from '../../assets/WhiteLogo.png';
// import {InitialScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';

//Styles
import GS from '../../styles';
import S, {styles} from './initial-screen.styled';
import {useStore} from '../../store';
import {observer} from 'mobx-react-lite';
interface InitialScreenProps {
  navigate: any;
}
const InitialScreen: React.FC<InitialScreenProps> = observer(({navigate}) => {
  const store = useStore();

  useEffect(() => {
    setTimeout(() => {
      store.auth.isSignedIn ? navigate('Home') : navigate('Auth');
    }, 3000);
  }, [navigate, store.auth.isSignedIn]);

  const [text, setText] = useState('');

  useEffect(() => {
    setInterval(() => {
      setTimeout(() => {
        setText('.');
      }, 200);
      setTimeout(() => {
        setText('..');
      }, 500);
      setTimeout(() => {
        setText('...');
      }, 800);
    }, 1500);
  }, []);

  return (
    <GS.SafeAreaView
      style={{backgroundColor: store.common.defaultTheme.background}}>
      <GS.LogoContainer>
        <GS.Logo
          source={
            store.common.defaultTheme.background === 'black' ? Logo : WhiteLogo
          }
        />
        <ImageBackground
          style={styles.tooltip}
          source={require('../../assets/Tooltip.png')}>
          <Text style={{color: 'white', fontSize: 9}}>Загрузка{text}</Text>
        </ImageBackground>
      </GS.LogoContainer>
      <S.BottomContainer>
        <S.BottomText style={{color: store.common.defaultTheme.textColor}}>
          Сделано компанией{' '}
          <S.BottomText style={styles.orange}>TEO</S.BottomText>
        </S.BottomText>
      </S.BottomContainer>
    </GS.SafeAreaView>
  );
});

export default InitialScreen;
