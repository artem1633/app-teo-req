// @ts-ignore
import styled from 'styled-components/native';
import {Text} from 'react-native-paper';
import {TextProps} from 'react-native';
import {FlatList as FlatListLib} from 'react-native';

const HeaderContainer = styled.View`
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  margin-top: 21.5px;
  margin-bottom: 16px;
`;
const EmptyContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex: 0;
  position: absolute;
  left: 0%;
  right: 0%;
  top: 20%;
`;
const EmptyText = styled(Text)<HeaderTextProps>`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  width: 70%;
  margin-top: 36px;
`;
interface HeaderTextProps extends TextProps {
  isActive: boolean;
}

const HeaderText = styled(Text)<HeaderTextProps>`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  border-bottom-width: ${({isActive}) => (isActive ? '1px' : '0px')};
  border-bottom-color: #f79827;
  padding-bottom: 4px;
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
`;
const FlatList = styled(FlatListLib as new () => FlatListLib)`
  margin-bottom: 10px;
`;

export default {
  HeaderContainer,
  HeaderText,
  EmptyContainer,
  EmptyText,
  FlatList,
};
