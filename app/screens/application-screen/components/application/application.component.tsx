import React, {useState} from 'react';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
// Styles
import S, {styles} from './application.styled';
import {
  TouchableOpacity,
  Animated,
  Image,
  Linking,
  View,
  Share,
} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import {SelectableText} from '@astrocoders/react-native-selectable-text';
import {observer} from 'mobx-react-lite';
import {useStore} from '../../../../store';
// @ts-ignore
import Highlighter from 'react-native-highlight-words';
//Image
import Img from '../../../../assets/Image.png';

interface ApplicationProps {
  item: any;
  changeReq: any;
  addKey: any;
  addChanelBlack: any;
  deleteChanel: any;
  section: string;
}

const Application: React.FC<ApplicationProps> = observer(
  ({item, changeReq, addKey, addChanelBlack, deleteChanel, section}) => {
    const renderLeftActions = () => {
      return (
        <RectButton
          style={{
            backgroundColor: '#46E6AC',
            width: 151,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animated.View>
            <Image
              source={require('../../../../assets/Like.png')}
              style={{position: 'absolute'}}
            />
            <S.SwipeText>Одобрить</S.SwipeText>
          </Animated.View>
        </RectButton>
      );
    };

    const renderRightActions = () => {
      return (
        <RectButton
          style={{
            backgroundColor: '#E64646',
            width: 151,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animated.View>
            <Image
              source={require('../../../../assets/Dislike.png')}
              style={{position: 'absolute'}}
            />
            <S.SwipeText>Отклонить</S.SwipeText>
          </Animated.View>
        </RectButton>
      );
    };
    const store = useStore();
    const refs = React.createRef();
    const [textHeight, setTextHeight] = useState<number | string>(80);
    const onShare = async (text: string, name: string, url: string) => {
      try {
        const result = await Share.share({
          message: `Название канала: \n${name}\n \nСсылка на канал: \n${url}\n \nТекст заявки: \n${text}\n \nЗаказ найден с помощью TEO REQ: \nСкачать приложение можно по ссылке: \nAndroid: https://play.google.com/store/apps/details?id=com.appteo\nIOS: https://apps.apple.com/us/app/teo-req/id1549501217\n`,
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {}
    };
    return (
      <Swipeable
        containerStyle={{
          flex: 1,
          height: 'auto',
          marginBottom: 12,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2.65,

          elevation: 3,
          borderColor: store.common.defaultTheme.background,
          borderWidth: 1,
          borderTopColor: 1,
        }}
        onSwipeableLeftOpen={() => changeReq(item.id, 1)}
        onSwipeableRightOpen={() => changeReq(item.id, 2)}
        renderLeftActions={renderLeftActions}
        renderRightActions={renderRightActions}>
        <S.Container
          ref={refs}
          style={{
            backgroundColor: store.common.defaultTheme.inputColor,
            borderLeftWidth: section === 'approved' ? 10 : 0,
            borderRightWidth: section === 'not approved' ? 10 : 0,
            borderLeftColor: '#46E6AC',
            borderRightColor: '#E64646',
            paddingVertical: 10,
          }}>
          <S.Header>
            <S.Logo
              source={
                item.telegramChanel.img ? {uri: item.telegramChanel.img} : Img
              }
            />
            <S.HeaderTitle
              style={{color: store.common.defaultTheme.textColor}}
              numberOfLines={1}>
              {item.name}
            </S.HeaderTitle>
            <Menu>
              <MenuTrigger
                customStyles={{
                  triggerOuterWrapper: {
                    marginLeft: '88%',
                    marginTop: -10,
                  },
                }}>
                <S.Icon source={require('../../../../assets/icon.png')} />
              </MenuTrigger>
              <MenuOptions
                customStyles={{
                  optionsWrapper: {
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                  },
                  optionsContainer: {
                    backgroundColor:
                      store.common.defaultTheme.background === 'black'
                        ? '#1B1814'
                        : '#fff',
                    borderRadius: 8,
                    width: 170,
                    height: 'auto',
                    alignItems: 'center',
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 0,
                      height: 4,
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: 4.65,

                    elevation: 2,
                  },
                }}>
                <MenuOption
                  onSelect={() => deleteChanel(item.id)}
                  style={{
                    borderBottomWidth: 0.5,
                    borderBottomColor: '#9B9B9B',
                    width: '100%',
                  }}>
                  <S.MenuText
                    style={{color: store.common.defaultTheme.textColor}}>
                    Удалить
                  </S.MenuText>
                </MenuOption>
                <MenuOption
                  onSelect={() =>
                    onShare(
                      item.text,
                      item.name,
                      `http://t.me/${item.telegramChanel.url}`,
                    )
                  }
                  style={{
                    borderBottomWidth: 0.5,
                    borderBottomColor: '#9B9B9B',
                    width: '100%',
                  }}>
                  <S.MenuText
                    style={{color: store.common.defaultTheme.textColor}}>
                    Поделится
                  </S.MenuText>
                </MenuOption>
                <MenuOption
                  onSelect={() => addChanelBlack(item.username)}
                  style={{
                    width: '100%',
                  }}>
                  <S.MenuText
                    style={{color: store.common.defaultTheme.textColor}}>
                    В черный список
                  </S.MenuText>
                </MenuOption>
              </MenuOptions>
            </Menu>
          </S.Header>
          <S.Subheading>
            <S.Username>@{item.username || item.name}</S.Username>
            <S.Count style={{color: store.common.defaultTheme.textColor}}>
              {item.subscribersCount} чел.
            </S.Count>
          </S.Subheading>
          {/* <S.Keyword>{item.value}</S.Keyword> */}
          <SelectableText
            style={{
              ...styles.ApplicationText,
              height: textHeight,
              flex: 1,
              color: store.common.defaultTheme.textColor,
            }}
            menuItems={['- Отрицательные', '+ Положительные', 'Копировать']}
            onSelection={async ({eventType, content}) => {
              await addKey(eventType, content);
            }}
            TextComponent={() => (
              <Highlighter
                highlightStyle={{color: '#fc3c3c'}}
                searchWords={store.common.myKey.data.keyOne.map(
                  (text: any) => `${text.value}`,
                )}
                textToHighlight={item.text}
              />
            )}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => setTextHeight(textHeight === 80 ? 'auto' : 80)}>
              <S.GoToMessage>
                {textHeight === 80
                  ? 'ПОКАЗАТЬ ТЕКСТ ПОЛНОСТЬЮ'
                  : 'СКРЫТЬ ТЕКСТ'}
              </S.GoToMessage>
            </TouchableOpacity>
            <S.GoToMessageBtn
              onPress={() =>
                Linking.openURL(
                  `http://t.me/${item.telegramChanel.url}/${item.textId}`,
                )
              }>
              <Image source={require('../../../../assets/VectorRight.png')} />
            </S.GoToMessageBtn>
          </View>
        </S.Container>
      </Swipeable>
    );
  },
);

function arePropsEqual(
  prevProps: ApplicationProps,
  nextProps: ApplicationProps,
) {
  return nextProps.item.id === prevProps.item.id;
}

export default React.memo(Application, arePropsEqual);
