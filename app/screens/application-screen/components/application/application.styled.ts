// @ts-ignore
import styled from 'styled-components/native';
import {Text, Title} from 'react-native-paper';
import {StyleSheet} from 'react-native';
const Container = styled.View`
  padding-left: 32px;
  padding-top: 14px;
  padding-bottom: 10px;
  flex: 1;
`;
const Header = styled.View`
  justify-content: flex-start;
  flex-direction: row;
`;
const HeaderTitle = styled(Title)`
  font-style: normal;
  font-weight: 600;
  font-size: 17px;
  line-height: 22px;
  padding-left: 41px;
  position: absolute;
  width: 90%;
`;
const Icon = styled.Image`
  resize-mode: contain;
`;
const Subheading = styled.View`
  padding-left: 41px;
  justify-content: flex-start;
  flex-direction: row;
  bottom: 2.5px;
`;
const ApplicationText = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  letter-spacing: -0.32px;
  height: 30%;
  padding-right: 48px;
`;
const SwipeText = styled(Text)`
  font-weight: 600;
  font-size: 17px;
  line-height: 22px;
  /* identical to box height, or 129% */
  margin-left: 25px;
  letter-spacing: -0.408px;

  color: #ffffff;
`;

const GoToMessage = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;
  color: #3a728c;
`;
const GoToMessageBtn = styled.TouchableOpacity`
  width: 36px;
  height: 36px;
  background-color: #f79827;
  border-radius: 50px;
  margin-right: 16px;
  justify-content: center;
  align-items: center;
`;
const Username = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  color: #2f6db5;
`;
const Keyword = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #979797;
  margin-bottom: 4px;
`;

const Count = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  margin-left: 8px;
`;

const MenuText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  /* identical to box height, or 131% */
  letter-spacing: -0.32px;
  text-align: center;
`;
const Logo = styled.Image`
  position: absolute;
  width: 33px;
  height: 33px;
  border-radius: 50px;
  margin-top: 5px;
  resize-mode: contain;
`;
export const styles = StyleSheet.create({
  ApplicationText: {
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: -0.32,
    color: '#fff',
    paddingRight: 48,
    marginBottom: 5,
  },
});
export default {
  Container,
  Logo,
  ApplicationText,
  Header,
  HeaderTitle,
  Icon,
  Subheading,
  Username,
  Count,
  Keyword,
  GoToMessage,
  SwipeText,
  MenuText,
  GoToMessageBtn,
};
