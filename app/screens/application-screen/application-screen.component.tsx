import React, {useCallback, useLayoutEffect, useRef, useState} from 'react';
import {Dimensions, FlatList, Image} from 'react-native';
import {MenuProvider} from 'react-native-popup-menu';
import Clipboard from '@react-native-clipboard/clipboard';
//Style
import GS from '../../styles';
import S from './application-screen.styled';
import {ApplicationsScreenProps} from '../../navigators/root-stack-navigator/root-stack-navigator.component';
//Store
import {useStore} from '../../store';
//Components
import Application from './components/application';
import {observer} from 'mobx-react-lite';
import Loader from '../../components/loader';

const ApplicationScreen: React.FC<ApplicationsScreenProps> = observer(() => {
  const store = useStore();
  const [loader, setLoader] = useState<boolean>(true);
  const [content, setContent] = useState<string>('all');
  const [status, setStatus] = useState<number>(0);
  const [page, setPage] = useState<number>(1);
  const [refresh, setRefresh] = useState(false);
  const makeRemoteRequest = useCallback(async () => {
    await store.common.getApplicationNextList(page + 20, status).then(() => {
      setPage(page + 20);
      setRefresh(false);
    });
  }, [page, status, store.common]);
  const handleRefresh = useCallback(async () => {
    setRefresh(true);
    setPage(1);
    await store.common.getApplicationList(1, status).then(() => {
      setRefresh(false);
    });
  }, [status, store.common]);
  const handleLoadMore = useCallback(async () => {
    setRefresh(true);
    await makeRemoteRequest();
  }, [makeRemoteRequest]);

  const listRef = useRef<FlatList>(null);
  const listOffset = useRef<number | null>(null);
  const handleScroll = (e: any) => {
    listOffset.current = e.nativeEvent.contentOffset.y;
  };
  const handleContentSizeChange = (_: any, h: number) => {
    const {height} = Dimensions.get('window');
    // TODO: precise scroll behaviour
    if (h - ((listOffset.current || 0) + height) < 0) {
      setTimeout(() => {
        listRef.current?.scrollToEnd({animated: true});
      });
    }
  };
  const changeReq = useCallback(
    async (id: string, statusApplication: number) => {
      await store.common.changeReq(id, statusApplication);
      await store.common.getApplicationList(page, status);
    },
    [page, status, store.common],
  );
  const addKey = useCallback(
    async (value: string, text: string) => {
      if (value === 'Копировать') {
        Clipboard.setString(text);
        await Clipboard.getString();
      }
      value === '+ Положительные'
        ? await store.common.addKeyOne(text)
        : await store.common.addKeyMinus(text);
    },
    [store.common],
  );
  const addChanelBlack = useCallback(
    async (value) => {
      await store.common.addKeyChanelBlack(value);
    },
    [store.common],
  );
  const deleteApplication = useCallback(
    async (id) => {
      await store.common.deleteApplication(id);
    },
    [store.common],
  );
  // const measure = useCallback((refs) => {
  //   if (refs.current) {
  //     refs.current.measureInWindow((x: any, y: any) => {});
  //   }
  // }, []);
  const keyExtractor = useCallback((item) => `${item.id}`, []);
  const renderItem = useCallback(
    ({item}) => (
      <Application
        item={item}
        changeReq={changeReq}
        addKey={addKey}
        addChanelBlack={addChanelBlack}
        deleteChanel={deleteApplication}
        section={content}
      />
    ),
    [addChanelBlack, addKey, changeReq, content, deleteApplication],
  );

  return (
    <MenuProvider>
      <GS.SafeAreaView
        style={{backgroundColor: store.common.defaultTheme.background}}>
        <S.HeaderContainer>
          <S.HeaderText
            style={{
              color:
                content === 'all'
                  ? store.common.defaultTheme.textColor
                  : '#818C99',
            }}
            isActive={content === 'all'}
            onPress={async () => {
              setContent('all');
              setStatus(0);
              setPage(1);
              store.common.setStatus(0);
              await store.common.getApplicationList(1, 0).then(() => {
                setLoader(false);
              });
            }}>
            Новые
          </S.HeaderText>
          <S.HeaderText
            style={{
              color:
                content === 'approved'
                  ? store.common.defaultTheme.textColor
                  : '#818C99',
            }}
            isActive={content === 'approved'}
            onPress={async () => {
              setContent('approved');
              setStatus(1);
              setPage(1);
              store.common.setStatus(1);
              await store.common.getApplicationList(1, 1).then(() => {
                setLoader(false);
              });
            }}>
            Одобрены
          </S.HeaderText>
          <S.HeaderText
            style={{
              color:
                content === 'not approved'
                  ? store.common.defaultTheme.textColor
                  : '#818C99',
            }}
            isActive={content === 'not approved'}
            onPress={async () => {
              setContent('not approved');
              setStatus(2);
              setPage(1);
              store.common.setStatus(2);
              await store.common.getApplicationList(1, 2).then(() => {
                setLoader(false);
              });
            }}>
            Не одобрены
          </S.HeaderText>
        </S.HeaderContainer>
        <>
          <S.FlatList
            ref={listRef}
            onScroll={handleScroll}
            data={store.common.applicationList}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            onEndReachedThreshold={1}
            numColums={3}
            onEndReached={handleLoadMore}
            onRefresh={handleRefresh}
            refreshing={refresh}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            style={{
              height: store.common.applicationList.length > 0 ? 'auto' : 0,
            }}
          />
          {!store.common.applicationList?.length && (
            <S.EmptyContainer>
              <Image
                source={require('../../assets/Image.png')}
                style={{width: '80%', resizeMode: 'contain'}}
              />
              <S.EmptyText style={{color: store.common.defaultTheme.textColor}}>
                Пока заявок по вашим запросам не найдено. Скоро они появятся
              </S.EmptyText>
            </S.EmptyContainer>
          )}
        </>
      </GS.SafeAreaView>
    </MenuProvider>
  );
});
export default ApplicationScreen;
