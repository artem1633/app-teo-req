import {Observable} from 'rxjs';

export function createHttpObservable(url: string, init: RequestInit = {}) {
  return new Observable((observer) => {
    const controller = new AbortController();
    const initOptions: RequestInit = {
      signal: controller.signal,
      ...init,
    };

    fetch(url, initOptions)
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          return observer.error(res);
        }
      })
      .then((body) => {
        observer.next(body);
        observer.complete();
      })
      .catch((err) => {
        observer.error(err);
      });

    return () => {
      controller.abort();
    };
  });
}
