import AsyncStorage from '@react-native-community/async-storage';

export default class DataStorage {
  private static REFRESH_TOKEN_STORAGE_KEY: string = '@refresh_token';
  private static ACCESS_TOKEN_STORAGE_KEY: string = '@access_token';
  private static FIREBASE_TOKEN: string = '@firebase_token';
  private static THEME: string = '@theme';
  private static NEW_USER: string = '@new_user';

  static async storeRefreshToken(refreshToken: string): Promise<boolean> {
    return DataStorage.storeItem(this.REFRESH_TOKEN_STORAGE_KEY, refreshToken);
  }

  static async storeAccessToken(accessToken: string): Promise<boolean> {
    return DataStorage.storeItem(this.ACCESS_TOKEN_STORAGE_KEY, accessToken);
  }

  static async storeFirebaseToken(token: string): Promise<boolean> {
    return DataStorage.storeItem(this.FIREBASE_TOKEN, token);
  }

  static async storeTheme(theme: any): Promise<boolean> {
    return DataStorage.storeItem(this.THEME, theme);
  }

  static async getTheme(): Promise<any | null> {
    return DataStorage.getItem(DataStorage.THEME);
  }

  static async storeNewUser(user: any): Promise<boolean> {
    return DataStorage.storeItem(this.NEW_USER, user);
  }

  static async getNewUser(): Promise<any | null> {
    return DataStorage.getItem(DataStorage.NEW_USER);
  }

  static async getAccessToken(): Promise<string | null> {
    return DataStorage.getItem(DataStorage.ACCESS_TOKEN_STORAGE_KEY);
  }

  static async getFirebaseToken(): Promise<string | null> {
    return DataStorage.getItem(DataStorage.FIREBASE_TOKEN);
  }

  static async getRefreshToken(): Promise<string | null> {
    return DataStorage.getItem(DataStorage.REFRESH_TOKEN_STORAGE_KEY);
  }

  static async removeAccessToken(): Promise<boolean> {
    return DataStorage.removeItem(DataStorage.ACCESS_TOKEN_STORAGE_KEY);
  }

  static async removeRefreshToken(): Promise<boolean> {
    return DataStorage.removeItem(DataStorage.REFRESH_TOKEN_STORAGE_KEY);
  }

  static async storeItem(key: string, item: string | object): Promise<boolean> {
    try {
      await AsyncStorage.setItem(
        key,
        typeof item === 'string' ? item : JSON.stringify(item),
      );
      return true;
    } catch (err) {
      return false;
    }
  }

  static async getItem(key: string): Promise<string | null> {
    try {
      return await AsyncStorage.getItem(key);
    } catch (err) {
      return null;
    }
  }

  static async removeItem(key: string): Promise<boolean> {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (err) {
      return false;
    }
  }
}
